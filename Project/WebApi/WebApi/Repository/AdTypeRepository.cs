﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Repository
{
    public class AdTypeRepository : ContextRepository
    {
        public AdTypeRepository()
        {
        }

        public List<AdType> GetAllAdTypes()
        {
            var list = _ctx.AdType.ToList();

            return list;
        }

        public AdType GetAdTypeByID(int AdTypeId) { return _ctx.AdType.Find(AdTypeId); }


    }
}