﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApi.Repository
{
    public class GPSLocationRepository : ContextRepository
    {

        public GPSLocationRepository()
        {
        }

        public List<GPSLocation> GetAllLocations()
        {
            return _ctx.GPSLocation.ToList();
        }

        public GPSLocation GetGPSLocationByID(int GPSLocationId) { return _ctx.GPSLocation.Find(GPSLocationId); }

        public GPSLocation InsertGPSLocation(GPSLocation Location) { return _ctx.GPSLocation.Add(Location); }

        public void Update(GPSLocation updatedGPSLocation)
        {
            _ctx.Entry(updatedGPSLocation).State = EntityState.Modified;

        }

        public void DeleteGPSLocation(int id)
        {
            GPSLocation loc = _ctx.GPSLocation.Find(id);
            _ctx.GPSLocation.Remove(loc);
        }
    }
}