﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RealEstateLibrary.Models;
using Microsoft.AspNet.Identity;

namespace WebApi.Repository
{
    public class UserRepository : ContextRepository
    {

        public UserRepository()
        {
        }

        public List<ApplicationUser> GetAllUsers()
        {
            var list = _ctx.Users.ToList();

            return list;
        }

        public bool addVisitToUserAgenda(Visit visit)
        {
            IEnumerable<ApplicationUser> userInThisVisit = this.GetAllUsers().Where(s => s.UserName == visit.UserID);
            if (userInThisVisit.Count() > 0)
            {
                ApplicationUser user = userInThisVisit.ElementAt(0);
                if (user.Schedule.Where(s => s.VisitID == visit.VisitID).Count() == 0)
                {
                    user.Schedule.Add(visit);
                    return true;
                }
            } 
            return false;
        }

        public bool addVisitToUserIDAgenda(Visit visit, ApplicationUser user)
        {
            
            if (user.Schedule.Where(s => s.VisitID == visit.VisitID).Count() == 0)
            {
                user.Schedule.Add(visit);
                return true;
            }

            return false;
        }
            
    }
}