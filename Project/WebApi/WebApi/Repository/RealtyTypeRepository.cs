﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApi.Repository
{
    public class RealtyTypeRepository : ContextRepository
    {

        public RealtyTypeRepository()
        {
        }

        public List<RealtyType> GetAllRealtyTypes()
        {
            return _ctx.RealtyType.ToList();
        }

        public RealtyType GetRealtyTypeByID(int RealtyTypeId) { return _ctx.RealtyType.Find(RealtyTypeId); }

        public RealtyType InsertRealtyType(RealtyType RealtyType) { return _ctx.RealtyType.Add(RealtyType); }

        public void Update(RealtyType updatedRealtyType)
        {
            _ctx.Entry(updatedRealtyType).State = EntityState.Modified;

        }

        public void DeleteRealtyType(int id)
        {
            RealtyType rt = _ctx.RealtyType.Find(id);
            _ctx.RealtyType.Remove(rt);
        }
    
    }
}