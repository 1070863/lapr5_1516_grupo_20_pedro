﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RealEstateLibrary.Models;
using WebApi.Repository;
using WebApi.ViewModels;
using Web_API.Repository;
using WebApi.Service;
using System.Text;

namespace WebApi.Controllers
{
   // [Authorize]
    public class AdController : ApiController
    {
        private AdRepository Ad_repo = new AdRepository();
        private GPSLocationRepository GPSLocation_repo = new GPSLocationRepository();
        private RealtyTypeRepository realtyType_repo = new RealtyTypeRepository();
        private RealtyRepository realty_repo = new RealtyRepository();
        private AdTypeRepository adType_repo = new AdTypeRepository();
        private PhotoRepository photo_repo = new PhotoRepository();
        private AlertRepository alert_repo = new AlertRepository();
        private VerifyMatchNotification match = new VerifyMatchNotification();


        // GET: api/Ad
        public List<AdVM> GetAd()
        {

            var listAds = Ad_repo.GetAllAds();
            var listAdVMs = new List<AdVM>();

            for (var i = 0; i < listAds.Count; i++)
            {
                var adVM = AdToAdVM(listAds[i]);
                listAdVMs.Add(adVM);
            }

            return listAdVMs;

        }

        private AdVM AdToAdVM(Ad ad)
        {
            var adVM = new AdVM();
            var listURL = new List<String>();
            var listID = new List<int>();

            adVM.Address = ad.Realty.Address;
            adVM.AdID = ad.AdID;
            adVM.AdTypeDescription = ad.AdType.Description;
            adVM.AdTypeID = ad.AdTypeID;
            adVM.Altitude = ad.Realty.GPSLocation.Altitude;
            adVM.Area = ad.Realty.Area;
            adVM.Cost = ad.Cost;
            adVM.GPSLocationID = ad.Realty.GPSLocationID;
            adVM.Latitude = ad.Realty.GPSLocation.Latitude;
            adVM.Longitude = ad.Realty.GPSLocation.Longitude;
            adVM.MainTypeID = ad.Realty.RealtyType.MainTypeID;
            adVM.RealtyID = ad.RealtyID;
            adVM.RealtyTypeDescription = ad.Realty.RealtyType.Description;
            adVM.RealtyTypeID = ad.Realty.RealtyTypeID;
            adVM.User = ad.User;
            adVM.Mediator = ad.Mediator;
            adVM.Day = ad.Realty.Day;
            adVM.StartHour = ad.Realty.StartHour;
            adVM.EndHour = ad.Realty.EndHour;

            for (var j = 0; j < ad.Photos.Count; j++)
            {
                listURL.Add(ad.Photos[j].URL);
                listID.Add(ad.Photos[j].PhotoID);
            }

            adVM.Photo = listURL;
            adVM.PhotoID = listID;

            return adVM;

        }

        // GET: api/Ad/5
        [ResponseType(typeof(AdVM))]
        public IHttpActionResult GetAd(int id)
        {

            Ad ad = Ad_repo.GetAdByID(id);
            if (ad == null)
            {
                return NotFound();
            }

            var adVM = AdToAdVM(ad);

            return Ok(adVM);

        }

        // PUT: api/Ad/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAd(int id, AdVM adVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != adVM.AdID)
            {
                return BadRequest();
            }

            DeleteAd(adVM.AdID);

            PostAd(adVM);
           
            return StatusCode(HttpStatusCode.NoContent);

        }

        private Ad AdVMToAd(AdVM adVM)
        {

            var rt = new RealtyType();
            rt.Description = adVM.RealtyTypeDescription;
            if (adVM.MainTypeID != null)
            {
                rt.MainType = realtyType_repo.GetRealtyTypeByID((int)adVM.MainTypeID);
                rt.MainTypeID = adVM.MainTypeID;
            }
            rt.RealtyTypeID = adVM.RealtyTypeID;

            realtyType_repo._ctx.Entry(rt).State = EntityState.Modified;

            GPSLocation location = new GPSLocation();
            location.Altitude = adVM.Altitude;
            location.Latitude = adVM.Latitude;
            location.Longitude = adVM.Longitude;
            location.GPSLocationID = adVM.GPSLocationID;

            GPSLocation_repo._ctx.Entry(location).State = EntityState.Modified;

            Realty realty = new Realty();
            realty.Address = adVM.Address;
            realty.Area = adVM.Area;
            realty.GPSLocation = location;
            realty.RealtyTypeID = adVM.RealtyTypeID;
            realty.RealtyID = adVM.RealtyID;
            realty.GPSLocationID = location.GPSLocationID;
            realty.Day = adVM.Day;
            realty.EndHour = adVM.EndHour;
            realty.StartHour = adVM.StartHour;

            realty_repo._ctx.Entry(realty).State = EntityState.Modified;

            var ad = new Ad();
            ad.AdTypeID = adVM.AdTypeID;
            ad.Cost = adVM.Cost;
            ad.Realty = realty;
            ad.RealtyID = realty.RealtyID;
            ad.AdID = adVM.AdID;
            ad.User = User.Identity.Name;
            ad.Mediator = adVM.Mediator;
            
            

            for (var i=0; i<adVM.Photo.Count; i++)
            {
                if (adVM.Photo[i] != "")
                {
                    Photo p = new Photo();
                    p.URL = adVM.Photo[i];


                    ad.Photos.Add(p);
                }
            }

            return ad;

        }

        // POST: api/Ad
        [ResponseType(typeof(Ad))]
        public IHttpActionResult PostAd(AdVM adVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ad = AdVMToAd(adVM);

            Ad_repo.InsertAd(ad);
            Ad_repo._ctx.SaveChanges();
            Ad_repo.Subscribe(match);

            return CreatedAtRoute("DefaultApi", new { id = ad.AdID }, ad);

        }

        // DELETE: api/Ad/5
        [ResponseType(typeof(Ad))]
        public IHttpActionResult DeleteAd(int id)
        {

            Ad ad = Ad_repo.GetAdByID(id);
            if (ad == null)
            {
                return NotFound();
            }

            for (var i = 0; i < ad.Photos.Count; i++)
            {
                photo_repo.DeletePhoto(ad.Photos[i].PhotoID);

            }

            GPSLocation_repo.DeleteGPSLocation(ad.Realty.GPSLocationID);

            photo_repo._ctx.SaveChanges();
            GPSLocation_repo._ctx.SaveChanges();
            realty_repo._ctx.SaveChanges();
            Ad_repo._ctx.SaveChanges();

            return Ok(ad);

        }

        [Route("api/Ad/GetImoveisMediador/{IDMediador}")]
        public HttpResponseMessage GetImoveis(string IDMediador)
        {
            // cada um: id(dia,hora_inicio,hora_fim)
            // separados por ;
            string imoveis = realty_repo.GetIDRealtiesMediador(IDMediador);
            return new HttpResponseMessage()
            {
                Content = new StringContent(
                    imoveis,
                    Encoding.UTF8,
                    "text/html"
                )
            };
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Ad_repo._ctx.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AdExists(int id)
        {
            return Ad_repo._ctx.Ad.Count(e => e.AdID == id) > 0;
        }
    }
}