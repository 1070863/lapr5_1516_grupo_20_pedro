﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelsLibrary.ViewModels
{
    public class RoleVM
    {
        public String name { get; set; }
        public String user { get; set; }
    }
}
