:-consult(base_de_conhecimento).
:-consult(algoritmo_astar).

% Dado um conjunto de zonas, baseado no grafo existente criar um novo
% cujas liga��es entre as zonas s�o dadas pelo algoritmo a star.

:-dynamic zonah/2.
:-dynamic estradah/3.


%Metodo inicial para tranformar o grafo, usando o algoritmo a*star.
tranformar_grafo(Zonas):-
	                 retractall(estradah(_,_,_)),
			 for_each_zona(Zonas).


for_each_zona([]):-!.
for_each_zona([Id|L]):-criar_estradah(Id,L),!,for_each_zona(L).
for_each_zona([_|L]):-for_each_zona(L).


criar_estradah(_,[]).
criar_estradah(Id,[Id2|L]):-
			    hbf(Id,Id2,Percurso,Total),!,
			    NovaEstrada=..[estradah,Id,Id2,p_custo(Percurso,Total)],
			    assertz(NovaEstrada),
			    criar_estradah(Id,L).

criar_estradah(Id,[_|L]):-criar_estradah(Id,L).

%vizinho mais proximo
%
%Estes s�o os passos do algoritmo:

%escolha um v�rtice arbitr�rio como v�rtice atual.
% descubra a aresta de menor peso que seja conectada ao v�rtice atual e a
% um v�rtice n�o visitado V.
%fa�a o v�rtice atual ser V.
%marque V como visitado.
% se todos os v�rtices no dom�nio estiverem visitados, encerre o
% algoritmo. Se n�o v� para o passo 2.
%
%

menor_percurso(_,[X],X):-!.
menor_percurso(IdAtual,[Id|L],IdMenor):-menor_percurso(IdAtual,L,IdMenor),estradah(IdAtual,IdMenor,custo(_,Total)),estradah(IdAtual,Id,custo(_,Total1)),Total<Total1,!.
menor_percurso(_,[X|_],X).

vmp(Origem,Vertices,Percurso,Total):-
	    vmp1(Origem,Vertices,[],Percurso,0,Total).


vmp1(_,[],V,V,T,Total):-Total is T,!.
vmp1(X,NV,V,P,N,Total):-menor_percurso(X,NV,IdMenor),
	              estradah(X,IdMenor,p_custo(_,T)),
	              select(IdMenor,NV,NV2),
		      vmp1(IdMenor,NV2,[IdMenor|V],P,N+T,Total).
