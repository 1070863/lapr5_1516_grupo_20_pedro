% cidade(Cidade, PosX, PosY).
cidade(a,45,95).
cidade(b,90,95).
cidade(c,15,85).
cidade(d,40,80).
cidade(e,70,80).
cidade(f,25,65).
cidade(g,65,65).
cidade(h,45,55).
cidade(i,5,50).
cidade(j,80,50).
cidade(l,65,45).
cidade(m,25,40).
cidade(n,55,30).
cidade(o,80,30).
cidade(p,25,15).
cidade(q,80,15).
cidade(r,55,10).

estradah(a,b,45).
estradah(a,c,32).
estradah(a,d,16).
estradah(a,e,30).
estradah(b,e,25).
estradah(d,e,30).
estradah(c,d,26).
estradah(c,f,23).
estradah(c,i,37).
estradah(d,f,22).
estradah(f,h,23).
estradah(f,m,25).
estradah(f,i,25).
estradah(i,m,23).
estradah(e,f,48).
estradah(e,g,16).
estradah(e,j,32).
estradah(g,h,23).
estradah(g,l,20).
estradah(g,j,22).
estradah(h,m,25).
estradah(h,n,27).
estradah(h,l,23).
estradah(j,l,16).
estradah(j,o,20).
estradah(l,n,19).
estradah(l,o,22).
estradah(m,n,32).
estradah(m,p,25).
estradah(n,p,34).
estradah(n,r,20).
estradah(o,n,25).
estradah(o,q,15).
estradah(p,r,31).


menorValor([X],X):-!.
menorValor([X|L],Menor):-menorValor(L,Menor),
		       Menor<X,!.
menorValor([X|_],X).


menor_percursoh([X],X):-!.
menor_percursoh([c(F/G,_)|L],c(FM/GM,P)):-Menor=c(FM/GM,P),menor_percursoh(L,Menor),(FM-GM)<F,!.
%,F<FM,!.
%,M is FM+HM,X is F+H,M<X,!.
menor_percursoh([X|_],X).

hbf(Orig,Dest,Perc,Total):-
			estimativa(Orig,Dest,H), F is H + 0, % G = 0.
			hbf1([c(F/0,[Orig])],Dest,P,Total),
			reverse(P,Perc).
%select(?Elem, ?List1, ?List2).
hbf1(Percursos,Dest,Percurso,Total):-
	                %menor_percursoh(Percursos,Menor),Restantes),
                        menor_percursoh(Percursos,Menor),
			select(Menor,Percursos,Restantes),
			percursos_seguintesh(Menor,Dest,Restantes,Percurso,Total).

percursos_seguintesh(c(_/Dist,Percurso),Dest,_,Percurso,Dist):- Percurso=[Dest|_].
percursos_seguintesh(c(_,[Dest|_]),Dest,Restantes,Percurso,Total):-!,
	hbf1(Restantes,Dest,Percurso,Total).
percursos_seguintesh(c(_/Dist,[Ult|T]),Dest,Percursos,Percurso,Total):-
	findall(c(F1/D1,[Z,Ult|T]),proximo_noh(Ult,T,Z,Dist,Dest,F1/D1),Lista),
	append(Lista,Percursos,NovosPercursos),
	hbf1(NovosPercursos,Dest,Percurso,Total).


proximo_noh(X,T,Y,Dist,Dest,F/Dist1):-
					(estradah(X,Y,Z);estradah(Y,X,Z)),
					\+ member(Y,T),
					Dist1 is Dist + Z,
					estimativa(Y,Dest,H), F is H + Dist1.

estimativa(C1,C2,Est):-
		cidade(C1,X1,Y1),
		cidade(C2,X2,Y2),
		DX is X1-X2,
		DY is Y1-Y2,
		Est is sqrt(DX*DX+DY*DY).

% estimativa(_,_,0). % para desprezar a heurística.

