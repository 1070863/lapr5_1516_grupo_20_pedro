﻿/*Pretende-se desenvolver uma aplicação que informatize alguns processos de negócio duma imobiliária. 
Nesta primeira parte do desenvolvimento (iteração 1) pretende-se desenvolver um widget para ambiente Web que permita
 a procura de anúncios de imóveis. A procura de imóveis deve ser feita por facetas definidas pela informação 
 dos imóveis existentes na base de dados. A base de dados será acedida através duma API REST. 
 Para o desenvolvimento da aplicação usamos: javascript, DOM, AJAX*/

//variavéis globais
var namesRequest;
var valuesRequests = [];
var typeRequests = [];
var minRequests = [];
var maxRequests = [];
var refreshRequest;

var idAndTypeArray = [];

var selections;
var bolds = [];
var containerID = null;
var containerClass = null;
var table;

/*Esta função deve ser evocada pelo html da página hospedeira e tem como responsabilidade
evocar as funções de modo a gerar a página de forma automática, funcionando
como uma especie de "controller" das grandes tarefas a executar.
paramatro: id do objeto html 
onde o widget apresentará os dados*/
function openWidget() {
    if (document.getElementById(arguments[0]) != null) {
        containerID = arguments[0];
    } else {
        containerClass = arguments[0];
    }

    cleanContent();
    createTable();
    getNames();
    createCleanButton();
}

/*tem a responsabiliddae de criar um botão de limpar
recorrendo a DOM. */
function createCleanButton() {
    var cleanButton = document.createElement("button");
    var text = document.createTextNode("limpar");
    var label = document.createElement("label");
    cleanButton.setAttribute("id", "cleanButton");
    label.appendChild(text);
    cleanButton.appendChild(label);

    cleanButton.onclick = function () {
        if (containerID != null) {
            openWidget(containerID);
        } else {
            openWidget(containerClass);
        }
    }

    var div = document.createElement("div");
    div.setAttribute("id", "button");
    div.appendChild(cleanButton);
    writeComponentInTable(div);
}

/* tem a responsabilidade de limpar a área onde apresentará
os dados do widget*/
function cleanContent() {
    if (containerID != null) {
        var components = document.getElementById(containerID);
    } else {
        var components = document.getElementsByClassName(containerClass)[0];
    }
    while (components.firstChild) {
        components.removeChild(components.firstChild);
    }
}

/*Responsável por criar o objeto XMLHttpRequest.
Recorremos a AJax*/
function httpRequestObject() {
    try { return new XMLHttpRequest(); }
    catch (error) { }
    try { return new ActiveXObject("Msxml2.XMLHTTP"); }
    catch (error) { }
    try { return new ActiveXObject("Microsoft.XMLHTTP"); }
    catch (error) { }
}

/*Reponsável por criar dinamicamente a estrutura principal do widget
esta função cria objetos html e adiciona-os ao documento html*/
function createTable() {
    table = document.createElement("div");
    table.setAttribute("id", "table");
    var componentsColumn = document.createElement("td");
    var resultsColumn = document.createElement("td");
    table.appendChild(componentsColumn);
    table.appendChild(resultsColumn);
}

/*Responsável por fazer o pedido de facetas ao servidor
Para fazer o pedido recorremos a AJAX*/
function getNames() {
    namesRequest = httpRequestObject();

    if (namesRequest) {
        var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/facetas.php";
        namesRequest.onreadystatechange = namesXML;
        namesRequest.open("GET", encodeURI(url), true);
        namesRequest.send(null);
    }
}

/* Responsável por receber a resposta do pedido das facetas ao servidor
e enviar a lista das facetas para verifyType
Para receber a resposta recorremos a AJAX
a resposta é um objeto em xml com o nome das facetas*/
function namesXML() {
    if (namesRequest.readyState == 4 && namesRequest.status == 200) {

        var answer = namesRequest.responseXML;
        var namesList = answer.getElementsByTagName("faceta");

        verifyType(namesList);
    }
}

/*responsável por percorrer a lista de facetas e enviar o nome de cada faceta
para typerequest.
parametros: nomeList nome das facetas*/
function verifyType(namesList) {

    for (var i = 0; i < namesList.length; i++) {

        var name = namesList[i].childNodes[0].nodeValue;

        typeRequest(name, i);
    }
}

/*responsável por pedir o tipo de facetas ao servidor
esta função recebe o nome da faceta e faz o pedido ao servidor, sempre que 
é alterado o estado do pedido ele evoca a função typesJSON
Para fazer o pedido recorremos a AJAX
parametros: name (nome da faceta)
indice (indice da faceta) */
function typeRequest(name, indice) {

    typeRequests[indice] = httpRequestObject();

    var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/tipoFaceta.php?faceta=" + name;

    typeRequests[indice].onreadystatechange = function () {
        typesJSON(name, indice);
    }

    typeRequests[indice].open("GET", encodeURI(url), true);
    typeRequests[indice].send(null);
}

/*responsável por tratar os objetos da resposta ao pedido de tipo de facetas
dependendo do tipo de faceta evoca a função writeFacet ou writeFotosFacet
Para receber a resposta recorremos a AJAX
parametros: name (nome da faceta)
indice (indice da faceta)*/
function typesJSON(name, indice) {

    if (typeRequests[indice].readyState == 4 && typeRequests[indice].status == 200) {
        var content = JSON.parse(typeRequests[indice].responseText);
        var type = content.tipo;

        var semantic = content.semântica;

        if (semantic != "figura") {
            writeFacet(name, type, indice);
        } else {
            writeFotosFacet(name, indice);
        }
    }
}

/*Reponsável por criar o objeto html para as fotos
esta função cria o objeto html para as fotos 
e aplica também style de css ao objeto que cria
parametros: name (nome da faceta)
indice (indice da faceta)*/
function writeFotosFacet(name, indice) {
    var div = document.createElement("div");
    div.setAttribute("id", name);

    div.style.display = "none";
    div.style.visibility = "invisible";
    table.childNodes[1].appendChild(div);
}

/*Reponsável por criar o objeto html para as facetas
esta função cria o objeto html para as facetas  
e evoca funções showValue shideValues que vão aplicar
 estilos de css aos objetos criados
 Esta função cria também os objetos html para os filhos 
 das facetas para percorer os filhos das facetas nesta função
recorremos a DOM 
parametros: name (nome da faceta)
indice (indice da faceta)*/
function writeFacet(name, type, indice) {

    var div = document.createElement("div");
    div.setAttribute("id", name);
    div.setAttribute("title", type);

    var bold = document.createElement("b");
    bold.appendChild(document.createTextNode(name.replace(/_/g, " ")));

    div.appendChild(bold);

    bolds[indice] = bold;

    bolds[indice].onmousemove = function () {
        bolds[indice].style.color = "blue";
        bolds[indice].style.fontSize = "25px";
        bolds[indice].onmouseout = function () {
            bolds[indice].style.color = "black";
            bolds[indice].style.fontSize = "14px";
        }
        bolds[indice].onclick = function () {
            showOrHideValues(bolds[indice].parentNode.id);
        }
    }


    writeComponentInTable(div);

    getFacetValues(div, name, indice, type);
}
/*responsável por evocar funções quando muda o estado do pedido,
 mediante o tipo da faceta evocar funções diferentes,fazer o pedido para obter 
 as opções/objetos das facetas.
parametros: div (element div da faceta em html)
name (nome da faceta)
type (tipo da faceta)
para verificar o estado do pedido recorremos a AJAX*/
function getFacetValues(div, name, indice, type) {

    valuesRequests[indice] = httpRequestObject();

    var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/valoresFaceta.php?faceta=" + name;

    if (type == "alfanumérico") {
        valuesRequests[indice].onreadystatechange = function () {
            discreetValuesJSON(div, name, indice);
        }
    } else {
        valuesRequests[indice].onreadystatechange = function () {
            rangeValuesJSON(div, name, indice);
        }
    }

    valuesRequests[indice].open("GET", encodeURI(url), true);
    valuesRequests[indice].send(null);
}

/*responsável por receber e tratar objetos recebidos do pedido ao servidor
evoca a função createDiscreetValues
parametros: div (element div da faceta em html)
name (nome da faceta)
type (tipo da faceta)
para verificar o estado do pedido recorremos a AJAX*/
function discreetValuesJSON(div, name, indice) {
    if (valuesRequests[indice].readyState == 4 && valuesRequests[indice].status == 200) {
        var valuesList = JSON.parse(valuesRequests[indice].responseText);
        createDiscreetValues(valuesList, div);
    }
}


/*responsável por receber a resposta do servidor e tratar os objetos recebidos 
evoca a função getMinValue
parametros: div (element div da faceta em html)
name (nome da faceta)
type (tipo da faceta)
para verificar o estado do pedido recorremos a AJAX*/
function rangeValuesJSON(div, name, indice) {
    if (valuesRequests[indice].readyState == 4 && valuesRequests[indice].status == 200) {
        var valuesList = JSON.parse(valuesRequests[indice].responseText);
        getMinValue(valuesList, div, indice);
    }
}

/*responsável por criar as checkbox em html com recurso a DOM
parametros: valuesList (lista de objetos das facetas do tipo discreto)
div (objeto html da faceta)*/
function createDiscreetValues(valuesList, div) {
    var div3 = document.createElement("div");

    for (var i = 0; i < valuesList.length; i++) {
        if (valuesList[i] != "") {
            var div2 = document.createElement("div");
            div2.setAttribute("id", valuesList[i]);
            var label = document.createElement("label");
            var checkBox = document.createElement("input");

            var text = document.createTextNode(valuesList[i]);
            checkBox.setAttribute("type", "checkbox");
            checkBox.setAttribute("name", valuesList[i]);
            checkBox.setAttribute("value", valuesList[i]);

            checkBox.onchange = function () {
                checkSelections();
            }

            label.appendChild(checkBox);
            label.appendChild(text);
            div2.appendChild(label);

            div3.appendChild(div2);
        }
    }

    div3.style.display = "none";
    div3.style.visibility = "hidden";

    div.appendChild(div3);

    appendTable();
}
/*responsável fazer o pedido ao servidor para obter valor min da faceta
parametros: valuesList (lista de objetos das facetas do tipo contínuo) div (objeto html da faceta)
indice (posição do array minRequest)*/
function getMinValue(valuesList, div, indice) {
    minRequests[indice] = httpRequestObject();

    if (minRequests[indice]) {
        var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/minFaceta.php?facetaCont=" + div.id;
        minRequests[indice].onreadystatechange = function () {
            minValueJSON(valuesList, div, indice);
        }
        minRequests[indice].open("GET", encodeURI(url), true);
        minRequests[indice].send(null);
    }
}

/*responsável por receber a resposta de valores min das facetas enviada pelo servidor
e tratar essa informação. evoca a função getMaxValue
parametros: valuesList (lista de objetos das facetas do tipo contínuo) div (objeto html da faceta)
indice (posição do array minRequest) */
function minValueJSON(valuesList, div, indice) {
    if (minRequests[indice].readyState == 4 && minRequests[indice].status == 200) {
        var content = JSON.parse(minRequests[indice].responseText);
        var min = content.min;

        getMaxValue(valuesList, div, indice, min);
    }
}

/*responsável fazer o pedido ao servidor para obter valor max da faceta
parâmetros: valuesList (lista de objetos das facetas do tipo contínuo) div (objeto html da faceta)
indice (posição do array minRequest) min (valor minimo das facetas)*/
function getMaxValue(valuesList, div, indice, min) {
    maxRequests[indice] = httpRequestObject();

    if (maxRequests[indice]) {
        var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/maxFaceta.php?facetaCont=" + div.id;
        maxRequests[indice].onreadystatechange = function () {
            maxValueJSON(valuesList, div, indice, min);
        }
        maxRequests[indice].open("GET", encodeURI(url), true);
        maxRequests[indice].send(null);
    }
}

/*responsável por receber a resposta de valores max das facetas enviada pelo servidor
e tratar essa informação. evoca a função createRangeValues
parâmetros: valuesList (lista de objetos das facetas do tipo contínuo) div (objeto html da faceta)
indice (posição do array minRequest) */
function maxValueJSON(valuesList, div, indice, min) {
    if (maxRequests[indice].readyState == 4 && maxRequests[indice].status == 200) {
        var content = JSON.parse(maxRequests[indice].responseText);
        var max = content.max;

        createRangeValues(valuesList, div, min, max);
    }
}


/*responsável por criar o range de resultados das facetas de tipo contínuo
parâmetros:  valuesList (lista de objetos das facetas do tipo contínuo)
div (objeto html da faceta) max(valor máximo das facetas) min (valor mínimo das facetas)*/
function createRangeValues(valuesList, div, min, max) {

    var div2 = document.createElement("div");
    var label = document.createElement("label");
    var textMin = document.createTextNode("Min");
    var textMax = document.createTextNode("    Max");

    var dropMin = document.createElement("input");
    dropMin.setAttribute("type", "number");
    dropMin.setAttribute("min", min);
    dropMin.setAttribute("max", max);
    dropMin.setAttribute("value", min);


    var dropMax = document.createElement("input");
    dropMax.setAttribute("type", "number");
    dropMax.setAttribute("min", min);
    dropMax.setAttribute("max", max);
    dropMax.setAttribute("value", max);

    label.appendChild(textMin);
    label.appendChild(dropMin);
    label.appendChild(textMax);
    label.appendChild(dropMax);

    div2.appendChild(label);
    div2.style.display = "none";
    div2.style.visibility = "hidden";

    div.appendChild(div2);

    label.onchange = function () {
        dropMin.min = dropMin.value;
        dropMin.max = dropMax.value;
        dropMax.min = dropMin.value;
        dropMax.max = dropMax.value;
        checkSelections();
    }

    appendTable();

}

/*responsável por adicionar o objeto html das facetas ao do documento
para a execução desta tarefa recorremos a DOM
parâmetro div (elemento html)*/
function writeComponentInTable(div) {
    table.firstChild.appendChild(div);
}


/*resposável por esconder ou mostrar objetos das facetas em html
Para percorrer os filhos das facetas recorremos a DOM
parâmetros id (identificação da faceta)*/
function showOrHideValues(id) {
    var list = table.firstChild.children;
    for (var i = 0; i < list.length; i++) {
        if (list[i].id == id) {
            if (list[i].childNodes[1].style.visibility == "visible") {
                list[i].childNodes[1].style.display = "none";
                list[i].childNodes[1].style.visibility = "hidden";
            } else {
                list[i].childNodes[1].style.display = "block";
                list[i].childNodes[1].style.visibility = "visible";
            }
        }
    }
}

/*responsável por verificar as opções selecionadas pelo utilizador
para esta execução desta tarefa recorremos a DOM
evoca funções  cleanResultsArea requestRefresh */
function checkSelections() {
    var list = table.firstChild.children;

    var id;

    selections = "";

    for (var i = 0; i < list.length; i++) {

        var marker = 0;

        if (list[i].title == "alfanumérico") {

            var listDivs = list[i].childNodes[1].children;


            for (var j = 0; j < listDivs.length; j++) {
                var div = listDivs[j];
                var checkbox = div.firstChild.firstChild;

                if (checkbox.checked) {

                    if (marker == 0) {
                        selections += list[i].id;
                        id = list[i].id;
                        selections += "=[";
                        marker++;
                    }

                    selections += div.id;
                    selections += ",";
                }
                if ((marker != 0) && ((j + 1) == (listDivs.length))) {
                    selections += "]&";
                }
            }
        }

        if (list[i].title == "alfanumérico") {
            var listBoxes = list[i].childNodes[1].children;
            for (var k = 0; k < listBoxes.length; k++) {
                if (!listBoxes[k].firstChild.firstChild.checked && list[i].id != id) {
                    listBoxes[k].style.display = "none";
                    listBoxes[k].style.visibility = "invisible";
                }
            }
        }
    }

    cleanResultsArea();
    requestRefresh(selections);
}


/*responsável por fazer pedidos ao servidor de acordo com o selecionado pelo utilizador
para fazer o pedido recorremos a AJAX
parâmetro: selectionList (lista de opções selecionadas pelo utilizador) */
function requestRefresh(selectionList) {

    refreshRequest = httpRequestObject();

    var url = "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/imoveis.php?" + selectionList;

    refreshRequest.onreadystatechange = function () {

        refreshComponents(selectionList);
    }

    refreshRequest.open("GET", encodeURI(url), true);
    refreshRequest.send(null);
}


/*responsável por receber a resposta do servidor de acordo com o selecionado pelo utilizador
e tratar os dados. para verificar a recepção do pedido recorremos a AJAX 
parâmetro: selectionList (lista de opções selecionadas pelo utilizador)
evoca funções writeInResultsArea refreshDiscreetOptions refreshRangeOptions*/
function refreshComponents(selectionList) {

    if (refreshRequest.readyState == 4 && refreshRequest.status == 200) {
        var content = JSON.parse(refreshRequest.responseText);

        if (selectionList != "") {
            writeInResultsArea(content);
        }
        var list = table.firstChild.children;

        for (var i = 0; i < list.length; i++) {
            if (list[i].title == "alfanumérico") {

                refreshDiscreetOptions(content, list[i]);

            } else if (list[i].title == "numérico") {

                refreshRangeOptions(content, list[i]);
            }
        }

    }
}

/*responsável por limpar área de resultados*/
function cleanResultsArea() {
    var resultsList = table.childNodes[1].children;
    var tamanho = resultsList.length;
    var cont = 0;

    for (var i = 0; i < tamanho; i++) {

        if (table.childNodes[1].childNodes[i - cont].id == "result" || table.childNodes[1].childNodes[i - cont].id == "images") {
            table.childNodes[1].removeChild(table.childNodes[1].childNodes[i - cont]);
            cont++;
        }
    }
}

/*responsável por limpar área de resultados
e criar o objeto imagem no html se existirem imagens.
parâmetro: content (dados com os resultados)*/
function writeInResultsArea(content) {
    var str = "";
    var list = table.firstChild.children;
    var id;

    for (var i = 0; i < content.length; i++) {
        str = "";

        if (filterCostAndArea(content[i])) {

            for (var j = 0; j < list.length; j++) {
                if (list[j].id != "button") {
                    id = list[j].id;
                    var id2 = id.replace(/_/g, " ");

                    str += id2 + ": ";
                    str += content[i][id];

                    if (j != list.length - 1) {
                        str += ", ";
                    }
                }
            }

            var name = table.childNodes[1].firstChild.id;


            var arrayFotos = content[i][name];
            if (arrayFotos.length > 0) {
                var divImages = document.createElement("div");

                for (var k = 0; k < arrayFotos.length; k++) {
                    var image = document.createElement("img");
                    image.setAttribute("src", arrayFotos[k]);

                    image.onclick = function () {
                        window.open(this.src);
                    }


                    divImages.appendChild(image);
                }
            }

            var div = document.createElement("div");
            div.setAttribute("id", "result");
            var label = document.createElement("label");
            var text = document.createTextNode(str);
            label.appendChild(text);

            div.appendChild(label);

            if (arrayFotos.length > 0) {
                div.appendChild(divImages);
            }
            table.childNodes[1].appendChild(div);
        }
    }

    if (table.childNodes[1].children.length == 1) {
        var str = "O servidor não consegue obter resultados para a pesquisa!";
        var div = document.createElement("div");
        div.setAttribute("id", "result");
        var label = document.createElement("label");
        var text = document.createTextNode(str);
        label.appendChild(text);

        div.appendChild(label);

        table.childNodes[1].appendChild(div);
    }

    appendTable();
}


/*responsável por filtar os dados contínuos recebidos do servidor
parâmetros: result (dados contínuos recebidos do servidor) */
function filterCostAndArea(result) {

    var list = table.firstChild.children;
    for (var i = 0; i < list.length; i++) {
        if (list[i].id == "preço") {
            var resultCost = result[list[i].id];
            var minCost = list[i].childNodes[1].firstChild.childNodes[1].min;
            var maxCost = list[i].childNodes[1].firstChild.childNodes[1].max;

            if (parseInt(minCost, 10) > parseInt(resultCost, 10) || parseInt(resultCost, 10) > parseInt(maxCost, 10)) {
                return false;
            }
        } else if (list[i].id == "área") {
            var resultArea = result[list[i].id];
            var minArea = list[i].childNodes[1].firstChild.childNodes[1].min;
            var maxArea = list[i].childNodes[1].firstChild.childNodes[1].max;

            if (parseInt(minArea, 10) > parseInt(resultArea, 10) || parseInt(resultArea, 10) > parseInt(maxArea, 10)) {
                return false;
            }
        }
    }
    return true;
}

/* responsável por refrescar os dados contínuos apresentados ao utilizador
parâmetro: content (dados para formar o range)
elemento (elemento html onde vamos apresentar os dados))*/

function refreshRangeOptions(content, element) {

    var id = element.id;

    var min = element.childNodes[1].firstChild.childNodes[1].min;
    var max = element.childNodes[1].firstChild.childNodes[1].max;

    for (var i = 0; i < content.length; i++) {

        var value = content[i][id];
        if (i == 0) {
            min = value;
            max = value;
        }

        if (parseInt(value, 10) > parseInt(max, 10)) {
            max = value;
        }

        if (parseInt(value, 10) < parseInt(min, 10)) {
            min = value;
        }
    }

    var list = element.childNodes[1].firstChild.children;
    for (var j = 0; j < list.length; j++) {

        if (j == 0) {
            list[j].setAttribute("value", min);
            list[j].textContent = min;
        } else {
            list[j].setAttribute("value", max);
        }
        list[j].setAttribute("min", min);
        list[j].setAttribute("max", max);
    }

}
/* responsável por refrescar os dados discretos apresentados ao utilizador*/
function refreshDiscreetOptions(content, element) {
    var name = element.id;

    for (var i = 0; i < content.length; i++) {

        var id = content[i][name];
        var listDivs = element.childNodes[1].children;

        for (var k = 0; k < listDivs.length; k++) {
            if (listDivs[k].id == id) {
                listDivs[k].style.display = "block";
                listDivs[k].style.visibility = "visible";
            }
        }
    }
}
/*responsável por adicionar o nosso widget à página hospedeira
para tal recorremos a DOM*/
function appendTable() {
    if (containerID != null) {
        document.getElementById(containerID).appendChild(table);
    } else {
        document.getElementsByClassName(containerClass)[0].appendChild(table);
    }
    myStyle();
}

/* responsável por formatar os elementos com estilos de CSS
para tal recorremos a DOM*/
function myStyle() {


    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = 'layout.css';
    link.media = 'all';
    head.appendChild(link);
    table.setAttribute("class", "myclass");
    var listChilds = table.childNodes;
    var width = document.getElementById('table').parentNode.offsetWidth;
    var tdstyle;
    if (parseInt(width, 10) < parseInt(500, 10)) {
        tdstyle = 'td100';
    } else {
        tdstyle = 'td50';
    }
    for (var a = 0; a < listChilds.length; a++) {
        table.childNodes[a].setAttribute("class", tdstyle);
    }

    var divs = document.getElementById("table").getElementsByTagName("div");
    for (var i = 0; i < divs.length; i++) {
        divs[i].setAttribute("class", "myclass");
    }
    var result = document.getElementById("table").getElementsByTagName("result");

    var imgs = document.getElementById("table").getElementsByTagName("img");
    for (var i = 0; i < imgs.length; i++) {
        imgs[i].setAttribute("width", "50%");
        imgs[i].setAttribute("height", "auto");
    }
    window.addEventListener("resize", resize);
}

/*Esta função é evodada sempre que é alterada a width da janela e adapata a janela em função do width*/
function resize() {
    var width = document.getElementById('table').parentNode.offsetWidth;
    var listChilds = table.childNodes;

    var tdstyle;
    if (parseInt(width, 10) < parseInt(400, 10)) {
        tdstyle = 'td100';
    } else {
        tdstyle = 'td50';
    }

    for (var a = 0; a < listChilds.length; a++) {
        table.childNodes[a].setAttribute("class", tdstyle);
    }
}
