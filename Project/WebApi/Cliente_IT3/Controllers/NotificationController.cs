﻿using Cliente_IT3.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using Cliente_IT3.ViewModels;
using System.Threading.Tasks;


namespace Cliente_IT3.Controllers
{
    public class NotificationController : Controller
    {

        // GET: Notification
        public async Task<ActionResult> Index()
        {
           
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Notification");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var notifications =
                JsonConvert.DeserializeObject<IEnumerable<NotificationVM>>(content);
                var notificationsForUser = new List<NotificationVM>();
                for (int i = 0; i < notifications.Count(); i++) {
                    if (notifications.ElementAt(i).userName == WebApiHttpClient.userLogged.UserName)
                        notificationsForUser.Add(notifications.ElementAt(i));
                            }
               return View(notificationsForUser);
            }
            else
            {
               return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }
        // GET: api/Notification
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Notification/5
        public string Get(int id)
        {
            return "value";
        }



        // POST: api/Notification
        public void Post([FromBody]string value)
        {
        }


        // PUT: api/Notification/5
        public void Put(int id, [FromBody]string value)
        {

        }


        // GET: Notification/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Notification/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var notification = JsonConvert.DeserializeObject<NotificationVM>(content);
                if (notification == null) return HttpNotFound();
                return View(notification);
            }

               
            
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Alert/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [System.Web.Mvc.HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "NotificationID, menssage, userName, notified")] NotificationVM notificationVM)
        {
            try
            {
                notificationVM.notified = true;
                var client = WebApiHttpClient.GetClient();
                string notificationJSON = JsonConvert.SerializeObject(notificationVM);
                HttpContent content = new StringContent(notificationJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PutAsync("api/Notification/" + notificationVM.NotificationID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }




        // DELETE: api/Notification/5
        public void Delete(int id)
        {
        }
    }
}
