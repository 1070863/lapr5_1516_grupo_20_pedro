﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cliente_IT3.ViewModels;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Cliente_IT3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;

namespace Cliente_IT3.Controllers
{
    public class AlertController : Controller
    {
        // GET: Alert
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alert");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alerts =
                JsonConvert.DeserializeObject<IEnumerable<AlertVM>>(content);

                await setDescriptions(alerts);

                return View(alerts);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        private async Task<ActionResult> setDescriptions(IEnumerable<AlertVM> alerts)
        {
            var client = WebApiHttpClient.GetClient();

            for (var i = 0; i < alerts.Count(); i++)
            {
                if (alerts.ElementAt(i).AdTypeID != null)
                {
                    HttpResponseMessage response = await client.GetAsync("api/AdType/" + alerts.ElementAt(i).AdTypeID);
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        var adType = JsonConvert.DeserializeObject<AdTypeVM>(content);
                        if (adType == null) return HttpNotFound();

                        alerts.ElementAt(i).AdTypeDescription = adType.Description;
                    }
                }

                if (alerts.ElementAt(i).RealtyTypeID != null)
                {
                    HttpResponseMessage response = await client.GetAsync("api/RealtyType/" + alerts.ElementAt(i).RealtyTypeID);
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        var realtyType = JsonConvert.DeserializeObject<RealtyTypeVM>(content);
                        if (realtyType == null) return HttpNotFound();

                        alerts.ElementAt(i).RealtyTypeDescription = realtyType.Description;
                    }
                }
            }
            return Content("Sucesso");
        }

        private async Task<ActionResult> setAdTypeDescriptions(IEnumerable<AdTypeVM> ads)
        {
            var client = WebApiHttpClient.GetClient();

            for (var i = 0; i < ads.Count(); i++)
            {
                if (ads.ElementAt(i).AdTypeID != null)
                {
                    HttpResponseMessage response = await client.GetAsync("api/AdType/" + ads.ElementAt(i).AdTypeID);
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        var adType = JsonConvert.DeserializeObject<AdTypeVM>(content);
                        if (adType == null) return HttpNotFound();

                        ads.ElementAt(i).Description = adType.Description;
                    }
                }
            }
            return Content("Sucesso");
        }

        private async Task<ActionResult> setRealtyTypeDescriptions(IEnumerable<RealtyTypeVM> rts)
        {
            var client = WebApiHttpClient.GetClient();

            for (var i = 0; i < rts.Count(); i++)
            {
                if (rts.ElementAt(i).RealtyTypeID != null)
                {
                    HttpResponseMessage response = await client.GetAsync("api/RealtyType/" + rts.ElementAt(i).RealtyTypeID);
                    if (response.IsSuccessStatusCode)
                    {
                        string content = await response.Content.ReadAsStringAsync();
                        var realtyType = JsonConvert.DeserializeObject<RealtyTypeVM>(content);
                        if (realtyType == null) return HttpNotFound();

                        rts.ElementAt(i).Description = realtyType.Description;
                    }
                }
            }
            return Content("Sucesso");
        }

        // GET: Alert/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alert/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alert = JsonConvert.DeserializeObject<AlertVM>(content);
                if (alert == null) return HttpNotFound();

                var alerts = new List<AlertVM>();
                alerts.Add(alert);

                await setDescriptions(alerts);

                return View(alert);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }


        // GET: Alert/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/AdType");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var adts =
                JsonConvert.DeserializeObject<IEnumerable<AdTypeVM>>(content);

                ViewBag.AdTypeID = new SelectList(adts, "AdTypeID", "Description");
            }

            HttpResponseMessage response2 = await client.GetAsync("api/RealtyType");
            if (response2.IsSuccessStatusCode)
            {
                string content2 = await response2.Content.ReadAsStringAsync();
                var rts =
                JsonConvert.DeserializeObject<IEnumerable<RealtyTypeVM>>(content2);

                ViewBag.RealtyTypeID = new SelectList(rts, "RealtyTypeID", "Description");
            }
            return View();
        }

        // POST: Alert/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AlertVM alert)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string alertJSON = JsonConvert.SerializeObject(alert);
                HttpContent content = new StringContent(alertJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Alert", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Alert/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alert/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alert = JsonConvert.DeserializeObject<AlertVM>(content);
                if (alert == null) return HttpNotFound();

                HttpResponseMessage response2 = await client.GetAsync("api/AdType");
                if (response2.IsSuccessStatusCode)
                {
                    string content2 = await response2.Content.ReadAsStringAsync();
                    var adts =
                    JsonConvert.DeserializeObject<IEnumerable<AdTypeVM>>(content2);

                    ViewBag.AdTypeID = new SelectList(adts, "AdTypeID", "Description");
                }

                HttpResponseMessage response3 = await client.GetAsync("api/RealtyType");
                if (response3.IsSuccessStatusCode)
                {
                    string content3 = await response3.Content.ReadAsStringAsync();
                    var rts =
                    JsonConvert.DeserializeObject<IEnumerable<RealtyTypeVM>>(content3);

                    ViewBag.RealtyTypeID = new SelectList(rts, "RealtyTypeID", "Description");
                }

                return View(alert);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Alert/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AlertID, AdTypeID, RealtyTypeID, RealtyTypeDescription, Address, MaxCost, MaxArea, UserName, AdTypeDescription")] AlertVM alert)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string alertJSON = JsonConvert.SerializeObject(alert);
                HttpContent content = new StringContent(alertJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Alert/" + alert.AlertID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }


        // GET: Alert/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Alert/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var alert = JsonConvert.DeserializeObject<AlertVM>(content);
                if (alert == null) return HttpNotFound();

                var alerts = new List<AlertVM>();
                alerts.Add(alert);

                await setDescriptions(alerts);

                return View(alert);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Alert/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Alert/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

    }
}
