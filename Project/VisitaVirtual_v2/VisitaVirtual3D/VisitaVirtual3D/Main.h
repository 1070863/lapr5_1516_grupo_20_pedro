#pragma once


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string>
#include <iostream>

/*OpenGL includes*/
#include <GL/glut.h>
#include <GL/glaux.h>

/*OpenAL includes*/
#include <AL/alut.h>

/*Internal includes*/
#include "mathlib.h"
#include "studio.h"
//#include "mdlviewer.h"

/*Modularization includes*/
#include "Configs.h"
#include "Keys.h"
#include "Position.h"
#include "Object.h"
#include "Camera.h"
#include "State.h"
#include "Model.h"
#include "Textures.h"
#include "Draw.h"
#include "Light.h"
#include "Rain.h"
#include "Skybox.h"


void redisplayTopSubwindow(int width, int height);
void reshapeNavigateSubwindow(int width, int height);
void strokeCenterString(char *str, double x, double y, double z, double s);