#pragma once

#define MAX_PARTICLES 90000
#define WCX		640
#define WCY		480
#define RAIN	0

class Rain {

public:

	float slowdown = 5.0;
	float velocity = 0.0;
	float zoom = -10.0;
	float pan = 0.0;
	float tilt = 0.0;
	float hailsize = 0.1;

	int loop;
	int fall;
	bool rain_flag = false;


	typedef struct {
		// Life
		bool alive;	// is the particle alive?
		float life;	// particle lifespan
		float fade; // decay			
					// color
		float red;
		float green;
		float blue;
		// Position/direction
		float xpos;
		float ypos;
		float zpos;
		// Velocity/Direction, only goes down in y dir
		float vel;;
		// Gravity
		float gravity;
	}particles;

	// Paticle System
	particles par_sys[MAX_PARTICLES];

	void initParticles(int i);
	void init();
	void drawRain();
	void drawScene();
	void reshape(int w, int h);
	void idle();
};
