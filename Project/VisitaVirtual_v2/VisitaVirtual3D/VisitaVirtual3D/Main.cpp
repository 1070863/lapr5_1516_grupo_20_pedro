#include "Main.h"


#pragma comment (lib, "glaux.lib")    /* link with Win32 GLAUX lib */
#pragma comment (lib, "openAL32.lib")
#pragma comment (lib, "alut.lib")
#pragma comment (user, "Compiled on " __DATE__ " at " __TIME__)


extern "C" int read_JPEG_file(char *, char **, int *, int *, int *);
using namespace std;

/////////////////////////////////////
//variaveis globais

State estado;
Model modelo;
Rain rain;

char mazedata[NUM_PISOS + 1][MAZE_HEIGHT][MAZE_WIDTH + 1]; /*= {
  "                  ",
  " ******* ******** ",
  " *       *      * ",
  " * * *** * *    * ",
  " * **  * ** * * * ",
  " *     *      * * ",
  " *          *** * ",
  " *           *  * ",
  " *     * *** **** ",
  " * *   *   *    * ",
  " *   ****  *    * ",
  " ********  **** * ",
  " *            * * ",
  " *     *      * * ",
  " ** ** *    *** * ",
  " *   *      *   * ",
  " *******  **** ** ",
  "                  "
}*/




///////////////////////////////////
//// Redisplays



void redisplayTopSubwindow(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL
	glViewport(0, 0, (GLint) width, (GLint) height);
	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();	
	gluPerspective(60,(GLfloat)width/height,.5,100);
	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);

}


void reshapeNavigateSubwindow(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL
	glViewport(0, 0, (GLint) width, (GLint) height);
	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();	
	gluPerspective(estado.camera.getFov(),(GLfloat)width/height,0.1,50);
	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
}



void reshapeMainWindow(int width, int height)
{
	GLint w,h;
    w = (width-GAP*3)*.5;
    h = (height-GAP*2);    
    glutSetWindow(estado.getTopWindow());
    glutPositionWindow(GAP, GAP);
    glutReshapeWindow(w, h);
    glutSetWindow(estado.getNavigateWindow());
    glutPositionWindow(GAP+w+GAP, GAP);
    glutReshapeWindow(w, h);

}




void strokeCenterString(char *str,double x, double y, double z, double s)
{
	int i,n;
	
	n = strlen(str);
	glPushMatrix();
	glTranslated(x-glutStrokeLength(GLUT_STROKE_ROMAN,(const unsigned char*)str)*0.5*s,y-119.05*0.5*s,z);
	glScaled(s,s,s);
	for(i=0;i<n;i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN,(int)str[i]);
	glPopMatrix();

}

/////////////////////////////////////
//navigateSubwindow
void motionNavigateSubwindow(int x, int y)
{
	int dif;
	dif=y-modelo.getYMouse();
	if(dif>0){//olhar para baixo
		estado.camera.incDir_lat(-dif*RAD(EYE_ROTACAO));
		if(estado.camera.getDir_lat()<-RAD(45))
			estado.camera.incDir_lat(-RAD(45));
	}

	if(dif<0){//olhar para cima
		estado.camera.incDir_lat(abs(dif)*RAD(EYE_ROTACAO));
		if(estado.camera.getDir_lat()>RAD(45))
		  estado.camera.setDir_lat(RAD(45));
	}

	dif=x-modelo.getXMouse();

	if(dif>0){ //olhar para a direita
		estado.camera.incDir_long(-dif*RAD(EYE_ROTACAO));
		/*
		if(estado.camera.dir_long<modelo.objecto.dir-RAD(45))
			estado.camera.dir_long=modelo.objecto.dir-RAD(45);
		*/
	}
	if(dif<0){//olhar para a esquerda
		estado.camera.incDir_long(abs(dif)*RAD(EYE_ROTACAO));
		/*
		if(estado.camera.dir_long>modelo.objecto.dir+RAD(45))
		estado.camera.dir_long=modelo.objecto.dir+RAD(45);
		*/

	}
	modelo.setXMouse(x);
	modelo.setYMouse(y);

}

void mouseNavigateSubwindow(int button, int state, int x, int y)
{
  if(button==GLUT_RIGHT_BUTTON)
  {
    if(state==GLUT_DOWN)
    {
      modelo.setXMouse(x);
      modelo.setYMouse(y);
      glutMotionFunc(motionNavigateSubwindow);
    }
    else
      glutMotionFunc(NULL);
  }
}

void setNavigateSubwindowCamera(Camera *cam, Object obj)
{

  Position center;

	if(estado.vista[JANELA_NAVIGATE])
  {
    cam->eye.setPosition(obj.pos.getX(), obj.pos.getY() + .2, obj.pos.getZ());
  
	center.setPosition(obj.pos.getX() + cos(cam->getDir_long())*cos(cam->getDir_lat()),
		cam->eye.getY() + sin(cam->getDir_lat()),
		obj.pos.getZ() + sin(-cam->getDir_long())*cos(cam->getDir_lat()));

  }
  else
  {
	  center.setPosition(obj.pos.getX(), obj.pos.getY() + 0.5, obj.pos.getZ());
 
	  cam->eye.setPosition(center.getX()- cos(cam->getDir_long()), center.getY() + 0.5, center.getZ() - sin(-cam->getDir_long()));


  }
  gluLookAt(cam->eye.getX(),cam->eye.getY(),cam->eye.getZ(),center.getX(),center.getY(),center.getZ(),0,1,0);
}

void displayNavigateSubwindow()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glLoadIdentity();

	setNavigateSubwindowCamera(&estado.camera, modelo.objecto);
  //setLight();

	glCallList(modelo.imovel[JANELA_NAVIGATE]);
	glCallList(modelo.chao[JANELA_NAVIGATE]);

	if(!estado.vista[JANELA_NAVIGATE])
  {
    glPushMatrix();
		  glTranslatef(modelo.objecto.pos.getX(),modelo.objecto.pos.getY(),modelo.objecto.pos.getZ());
		  glRotatef(GRAUS(modelo.objecto.getDir()),0,1,0);
      glRotatef(-90,1,0,0);
      glScalef(SCALE_HOMER,SCALE_HOMER,SCALE_HOMER);
      mdlviewer_display(modelo.homer[JANELA_NAVIGATE]);
	  glPopMatrix();
  }
	
	desenhaBussola(glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT), estado);

	glutSwapBuffers();
}

/////////////////////////////////////
//topSubwindow
void setTopSubwindowCamera( Camera *cam, Object obj)
{
	cam->eye.setX(obj.pos.getX());
	cam->eye.setZ(obj.pos.getZ());
	if(estado.vista[JANELA_TOP])
		gluLookAt(obj.pos.getX(),CHAO_DIMENSAO*.2,obj.pos.getZ(),obj.pos.getX(),obj.pos.getY(),obj.pos.getZ(),0,0,-1);
	else
		gluLookAt(obj.pos.getX(),CHAO_DIMENSAO*2,obj.pos.getX(),obj.pos.getX(),obj.pos.getY(),obj.pos.getZ(),0,0,-1);
}

void displayTopSubwindow()
{
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();
    setTopSubwindowCamera(&estado.camera,modelo.objecto);
    setLight(estado);

    glCallList(modelo.imovel[JANELA_TOP]);
    glCallList(modelo.chao[JANELA_TOP]);

    glPushMatrix();		
        glTranslatef(modelo.objecto.pos.getX(),modelo.objecto.pos.getY(),modelo.objecto.pos.getZ());
        glRotatef(GRAUS(modelo.objecto.getDir()),0,1,0);
        glRotatef(-90,1,0,0);
        glScalef(SCALE_HOMER,SCALE_HOMER,SCALE_HOMER);
        mdlviewer_display(modelo.homer[JANELA_TOP]);
    glPopMatrix();

    desenhaAngVisao(&estado.camera);
    desenhaModeloDir(modelo.objecto,glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT));
    glutSwapBuffers();
}

/////////////////////////////////////
//mainWindow

void redisplayAll(void)
{
	glutSetWindow(estado.getMainWindow());
	glutPostRedisplay();
	glutSetWindow(estado.getTopWindow());
  glutPostRedisplay();
  glutSetWindow(estado.getNavigateWindow());
  glutPostRedisplay();
  
    
}

void displayMainWindow()
{
	glClearColor(0.8f, 0.8f, 0.8f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
	glutSwapBuffers();
	
}

void Timer(int value)
{
	GLfloat nx,nz;
	GLboolean andar=GL_FALSE;

	ALint state;
	alGetSourcei(estado.source[0], AL_SOURCE_STATE, &state);
	if (state == AL_STOPPED)
	{
		alSourcef(estado.source[0], AL_GAIN, 0.5f);
		alSourcei(estado.source[0], AL_BUFFER, estado.buffer[1]);
		alSourcePlay(estado.source[0]);
	}

	GLuint curr = GetTickCount( );
	float velocidade= modelo.objecto.getVel()*(curr - modelo.getPrev() )*0.001;

	if(modelo.homer[JANELA_NAVIGATE].GetSequence()!=20)
	{
		glutTimerFunc(estado.timer, Timer, 0);
	}
	else if(value<4500)
    {
	    glutTimerFunc(estado.timer, Timer, value+curr - modelo.getPrev());
		redisplayAll();
		return;
    }
	
    else
    {
		modelo.homer[JANELA_NAVIGATE].SetSequence( 0 );
		modelo.homer[JANELA_TOP].SetSequence( 0 );
		glutTimerFunc(estado.timer, Timer, 0);
    }
	modelo.setPrev(curr);

    
  if(estado.teclas.getUp()){
		nx=modelo.objecto.pos.getX()+cos(modelo.objecto.getDir())*velocidade;
		nz=modelo.objecto.pos.getZ()+sin(-modelo.objecto.getDir())*velocidade;
		if(!detectaColisao(nx+cos(modelo.objecto.getDir())*OBJECTO_RAIO,nz+sin(-modelo.objecto.getDir())*OBJECTO_RAIO, mazedata, estado, modelo) &&
		   !detectaColisao(nx+cos(modelo.objecto.getDir() +M_PI/4)*OBJECTO_RAIO,nz+sin(-modelo.objecto.getDir() +M_PI/4)*OBJECTO_RAIO, mazedata, estado, modelo) &&
		   !detectaColisao(nx+cos(modelo.objecto.getDir() -M_PI/4)*OBJECTO_RAIO,nz+sin(-modelo.objecto.getDir() -M_PI/4)*OBJECTO_RAIO, mazedata, estado, modelo) ){
			modelo.objecto.pos.setX(nx);
			modelo.objecto.pos.setZ(nz);
		}
    andar=GL_TRUE;
	}
	if(estado.teclas.getDown()){
		nx=modelo.objecto.pos.getX()-cos(modelo.objecto.getDir())*velocidade;
		nz=modelo.objecto.pos.getZ()-sin(-modelo.objecto.getDir())*velocidade;
		if(!detectaColisao(nx,nz,mazedata, estado, modelo) &&
		   !detectaColisao(nx-cos(modelo.objecto.getDir() +M_PI/4)*OBJECTO_RAIO,nz-sin(-modelo.objecto.getDir() +M_PI/4)*OBJECTO_RAIO, mazedata, estado, modelo) &&
		   !detectaColisao(nx-cos(modelo.objecto.getDir() -M_PI/4)*OBJECTO_RAIO,nz-sin(-modelo.objecto.getDir() -M_PI/4)*OBJECTO_RAIO, mazedata, estado, modelo) ){
			modelo.objecto.pos.setX(nx);
			modelo.objecto.pos.setZ(nz);
		}
    andar=GL_TRUE;
	}
	if(estado.teclas.getLeft()){
		modelo.objecto.incDir(RAD(OBJECTO_ROTACAO));
		estado.camera.incDir_long(RAD(OBJECTO_ROTACAO));
	}
	if(estado.teclas.getRight()){
		modelo.objecto.incDir(-RAD(OBJECTO_ROTACAO));
		estado.camera.incDir_long(-RAD(OBJECTO_ROTACAO));
	}
	if (modelo.getSubir() && estado.teclas.getUp())
	{
		modelo.objecto.pos.setY(modelo.objecto.pos.getY() + 0.02);
		modelo.objecto.pos.setX(modelo.objecto.pos.getX() + 0.02);
	}

	else if (modelo.getSubir() && estado.teclas.getDown())
	{
		modelo.objecto.pos.setY(modelo.objecto.pos.getY() - 0.02);
		modelo.objecto.pos.setX(modelo.objecto.pos.getX() - 0.02);
	}
	if (modelo.getDescer() && estado.teclas.getUp())
	{
		modelo.objecto.pos.setY(modelo.objecto.pos.getY() - 0.02);
		modelo.objecto.pos.setX(modelo.objecto.pos.getX() - 0.02);
	}
	else if (modelo.getDescer() && estado.teclas.getDown())
	{
		modelo.objecto.pos.setY(modelo.objecto.pos.getY() + 0.02);
		modelo.objecto.pos.setX(modelo.objecto.pos.getX() + 0.02);
	}
	if (modelo.objecto.pos.getY()< OBJECTO_ALTURA*.5 - 0.41) {
		modelo.objecto.pos.setY(OBJECTO_ALTURA*.5 + 0.1);
		modelo.setDescer(GL_FALSE);
		modelo.setSubir(GL_FALSE);
		if (modelo.pisoActual>0)
		{
			modelo.pisoActual = 0;

		}

	}
	else if (modelo.objecto.pos.getY()> OBJECTO_ALTURA*.5 - 0.41 + 2)
	{
		modelo.objecto.pos.setY(OBJECTO_ALTURA*.5 + 1.5);
		modelo.setDescer(GL_FALSE);
		modelo.setSubir(GL_FALSE);
		modelo.pisoActual = 1;
	}


	if(modelo.homer[JANELA_NAVIGATE].GetSequence()!=20){
	  ALint state;
	  alGetSourcei(estado.source[1], AL_SOURCE_STATE, &state);
	  if (state != AL_PLAYING && rand() % 200 == 0)
	  {
		  if (andar)
			alSourcei(estado.source[1], AL_BUFFER, estado.buffer[5]);
		  else
			alSourcei(estado.source[1], AL_BUFFER, estado.buffer[2]);
		  alSourcePlay(estado.source[1]);
	  }
	if(andar && modelo.homer[JANELA_NAVIGATE].GetSequence()!=3)
    {
        modelo.homer[JANELA_NAVIGATE].SetSequence( 3 );
        modelo.homer[JANELA_TOP].SetSequence( 3 );
    }
    else
    if(!andar && modelo.homer[JANELA_NAVIGATE].GetSequence()!=0)
      {
        modelo.homer[JANELA_NAVIGATE].SetSequence( 0 );
        modelo.homer[JANELA_TOP].SetSequence( 0 );
      }
  }
	redisplayAll();
	
}

void Key(unsigned char key, int x, int y)
{
	
	switch (key) {
		case 27:
			exit(1);
			break;
    case 'h' :
    case 'H' :
                imprime_ajuda();
            break;
    case 'l':
    case 'L':
        estado.setLocalViewer(!estado.getLocalViewer());
        break;
    case 'w':
    case 'W':
          glutSetWindow(estado.getNavigateWindow());
          glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
          glDisable(GL_TEXTURE_2D);
          glutSetWindow(estado.getTopWindow());
          glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
          glDisable(GL_TEXTURE_2D);
        break;
    case 's':
    case 'S':
          glutSetWindow(estado.getNavigateWindow());
          glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
          glEnable(GL_TEXTURE_2D);
          glutSetWindow(estado.getTopWindow());
          glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
          glEnable(GL_TEXTURE_2D);
        break;
	case 'r':
	case 'R':
		
		rain.init();
		rain.fall = RAIN;
		rain.drawScene();

	}

}

void SpecialKey(int key, int x, int y)
{
	ALint state;
	switch (key) {
	case GLUT_KEY_UP: estado.teclas.setUp(GL_TRUE);
			break;
		case GLUT_KEY_DOWN: estado.teclas.setDown(GL_TRUE);
			break;
		case GLUT_KEY_LEFT: estado.teclas.setLeft(GL_TRUE);
			break;
		case GLUT_KEY_RIGHT: estado.teclas.setRight(GL_TRUE);
			break;
		case GLUT_KEY_F1:
			estado.vista[JANELA_TOP]=!estado.vista[JANELA_TOP];
			alGetSourcei(estado.source[2], AL_SOURCE_STATE, &state);
			if (state != AL_PLAYING)
			{
				alSourcei(estado.source[2], AL_BUFFER, estado.buffer[3]);
				alSourcePlay(estado.source[2]);
			}
			break;
		case GLUT_KEY_F2:
			estado.vista[JANELA_NAVIGATE]=!estado.vista[JANELA_NAVIGATE];
			alGetSourcei(estado.source[2], AL_SOURCE_STATE, &state);
			if (state != AL_PLAYING)
			{
				alSourcei(estado.source[2], AL_BUFFER, estado.buffer[4]);
				alSourcePlay(estado.source[2]);
			}
			break;
		case GLUT_KEY_PAGE_UP: 
                  if(estado.camera.getFov()>20)
                  {
                    estado.camera.incFov(-1);
                    glutSetWindow(estado.getNavigateWindow());
                    reshapeNavigateSubwindow(glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT));
                    redisplayAll();
                  }
			break;
		case GLUT_KEY_PAGE_DOWN: 
                  if(estado.camera.getFov()<130)
                  {
                    estado.camera.incFov(1);
                    glutSetWindow(estado.getNavigateWindow());
                    reshapeNavigateSubwindow(glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT));
                    redisplayAll();
                  }
			break;
	}

}

// Callback para interaccao via teclas especiais (largar na tecla)
void SpecialKeyUp(int key, int x, int y)
{
  switch (key) {
    case GLUT_KEY_UP: estado.teclas.setUp(GL_FALSE);
            break;
    case GLUT_KEY_DOWN: estado.teclas.setDown(GL_FALSE);
            break;
    case GLUT_KEY_LEFT: estado.teclas.setLeft(GL_FALSE);
            break;
    case GLUT_KEY_RIGHT: estado.teclas.setRight(GL_FALSE);
            break;
  }
}

////////////////////////////////////
// Inicializa��es



void createDisplayLists(int janelaID)
{
	modelo.imovel[janelaID]=glGenLists(2);
	glNewList(modelo.imovel[janelaID], GL_COMPILE);
		glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT );
		desenhaImovel(modelo.texID[janelaID][ID_TEXTURA_PAREDES], mazedata);

		glPopAttrib();
	glEndList();

	modelo.chao[janelaID]=modelo.imovel[janelaID]+2;
	glNewList(modelo.chao[janelaID], GL_COMPILE);
		glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT );
			desenhaChao(CHAO_DIMENSAO,modelo.texID[janelaID][ID_TEXTURA_CHAO],0,modelo);
				if (janelaID == JANELA_NAVIGATE) {
					glPushMatrix();
						glTranslatef(0, -.65, 0);
						desenhaTeto(TETO_DIMENSAO, modelo.texID[janelaID][ID_TEXTURA_TETO], 0,modelo);
					glPopMatrix();
					//2�piso
					glPushMatrix();
						glTranslatef(0, 1.31, 0);
						desenhaTeto(TETO_DIMENSAO, modelo.texID[janelaID][ID_TEXTURA_TETO], 1, modelo);
						glTranslatef(0, 0.01, 0);
						desenhaChao(CHAO_DIMENSAO, modelo.texID[janelaID][ID_TEXTURA_CHAO], 1, modelo);
					glPopMatrix();
				}
		glPopAttrib();
	glEndList();
}


void init()
{
  GLfloat amb[] = { 0.3f, 0.3f, 0.3f, 1.0f };

	estado.timer=100;	
	estado.camera.eye.setPosition(0, OBJECTO_ALTURA * 2, 0);
	
	estado.camera.setDir_long(0);
	estado.camera.setDir_lat(0);
	estado.camera.setFov(60);

	estado.setLocalViewer(1);
	estado.vista[JANELA_TOP]=0;
	estado.vista[JANELA_NAVIGATE]=0;

	modelo.objecto.pos.setPosition(0, OBJECTO_ALTURA*.5, 0);
	
	modelo.objecto.setDir(0);
	modelo.objecto.setVel(OBJECTO_VELOCIDADE);

	modelo.setXMouse(-1);
	modelo.setYMouse(-1);
    modelo.setAndar(GL_FALSE);
	modelo.setDescer(GL_FALSE);
	modelo.setSubir(GL_FALSE);
	modelo.pisoActual = 0;
	modelo.posicaoAnterior = ' ';
	modelo.dia = GL_FALSE;

	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_NORMALIZE);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT,amb);
}
void promptFile() {
	cout << "Introduza o caminho do ficheiro a utilizar\n";
	string path;
	cin >> path;
}


////////////////////////////////////
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitWindowPosition(10, 10);
    glutInitWindowSize(800+GAP*3, 400+GAP*2);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	estado.setMainWindow(glutCreateWindow("Real Estate - LAPR5 | Grupo 20 | 2016"));
	promptFile();
	importPlant("planta.txt", 0,modelo,mazedata);
	importPlant("planta1.txt", 1, modelo, mazedata);
    imprime_ajuda();

    // Registar callbacks do GLUT da janela principal
    init();
    glutReshapeFunc(reshapeMainWindow);
    glutDisplayFunc(displayMainWindow);

    glutTimerFunc(estado.timer,Timer,0);
    glutKeyboardFunc(Key);
    glutSpecialFunc(SpecialKey);
    glutSpecialUpFunc(SpecialKeyUp);

    // criar a sub window 
    estado.setTopWindow(glutCreateSubWindow(estado.getMainWindow(), GAP, GAP, 400, 400));
    init();
    setLight(estado);
    setMaterial();
    createTextures(modelo.texID[JANELA_TOP]);
    createDisplayLists(JANELA_TOP);

    mdlviewer_init( "putin.mdl", modelo.homer[JANELA_TOP] );
	

    glutReshapeFunc(redisplayTopSubwindow);
    glutDisplayFunc(displayTopSubwindow);

    glutTimerFunc(estado.timer,Timer,0);
    glutKeyboardFunc(Key);
    glutSpecialFunc(SpecialKey);
    glutSpecialUpFunc(SpecialKeyUp);


    estado.setNavigateWindow(glutCreateSubWindow(estado.getMainWindow(), 400+GAP, GAP, 400, 800));
    init();
    setLight(estado);
    setMaterial();

    createTextures(modelo.texID[JANELA_NAVIGATE]);
    createDisplayLists(JANELA_NAVIGATE);
    mdlviewer_init( "putin.mdl", modelo.homer[JANELA_NAVIGATE] );

    glutReshapeFunc(reshapeNavigateSubwindow);
    glutDisplayFunc(displayNavigateSubwindow);
    glutMouseFunc(mouseNavigateSubwindow);

    glutTimerFunc(estado.timer,Timer,0);
    glutKeyboardFunc(Key);
    glutSpecialFunc(SpecialKey);
    glutSpecialUpFunc(SpecialKeyUp);

	srand((unsigned) time(NULL));

	alutInit (&argc, argv);

	estado.buffer[0] = alutCreateBufferFromFile("anykey.wav");
	//estado.buffer[1] = alutCreateBufferFromFile("the_days.wav");
	estado.buffer[1] = alutCreateBufferFromFile("ambient.wav");
	estado.buffer[2] = alutCreateBufferFromFile("boring.wav");
	estado.buffer[3] = alutCreateBufferFromFile("graduate.wav");
	estado.buffer[4] = alutCreateBufferFromFile("hmrcando.wav");
	estado.buffer[5] = alutCreateBufferFromFile("wahoo.wav");
	estado.buffer[6] = alutCreateBufferFromFile("bang_ow.wav");
	estado.buffer[7] = alutCreateBufferFromFile("doh2.wav");
	estado.buffer[8] = alutCreateBufferFromFile("dohaaa.wav");
	alGenSources(4, estado.source);
	alSourcei(estado.source[0], AL_BUFFER, estado.buffer[0]);
	alSourcePlay(estado.source[0]);

    glutMainLoop();
    return 0;
}
