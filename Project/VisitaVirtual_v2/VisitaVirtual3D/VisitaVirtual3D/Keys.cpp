#include "Keys.h"



Keys::Keys()
{
}


Keys::~Keys()
{
}

GLboolean Keys::getUp() {
	return this->up;
}
GLboolean Keys::getDown() {
	return this->down;
}
GLboolean Keys::getLeft() {
	return this->left;
}
GLboolean Keys::getRight() {
	return this->right;
}

void Keys::setUp(GLboolean up) {
	this->up = up;
}
void Keys::setDown(GLboolean down) {
	this->down = down;
}
void Keys::setLeft(GLboolean left) {
	this->left = left;
}
void Keys::setRight(GLboolean right) {
	this->right = right;
}
void imprime_ajuda(void)
{
	printf("\n\nDesenho de um quadrado\n");
	printf("h,H - Ajuda \n");
	printf("******* Diversos ******* \n");
	printf("l,L - Alterna o calculo luz entre Z e eye (GL_LIGHT_MODEL_LOCAL_VIEWER)\n");
	printf("w,W - Wireframe \n");
	printf("s,S - Fill \n");
	printf("******* Movimento ******* \n");
	printf("up  - Acelera \n");
	printf("down- Trava \n");
	printf("left- Vira rodas para a direita\n");
	printf("righ- Vira rodas para a esquerda\n");
	printf("******* Camara ******* \n");
	printf("F1 - Alterna camara da janela da Esquerda \n");
	printf("F2 - Alterna camara da janela da Direita \n");
	printf("PAGE_UP, PAGE_DOWN - Altera abertura da camara \n");
	printf("botao esquerdo + movimento na Janela da Direita altera o olhar \n");
	printf("ESC - Sair\n");
}


