#include "Draw.h"

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[], GLfloat xa, GLfloat ya, GLfloat xb, GLfloat yb, GLfloat xc, GLfloat yc, GLfloat xd, GLfloat yd)
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	glTexCoord2f(xa, ya);
	glVertex3fv(a);
	glTexCoord2f(xb, yb);
	glVertex3fv(b);
	glTexCoord2f(xc, yc);
	glVertex3fv(c);
	glTexCoord2f(xd, yd);
	glVertex3fv(d);
	glEnd();
}
void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[], GLfloat tx, GLfloat ty)
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	glTexCoord2f(tx + 0, ty + 0);
	glVertex3fv(a);
	glTexCoord2f(tx + 0, ty + 0.25);
	glVertex3fv(b);
	glTexCoord2f(tx + 0.25, ty + 0.25);
	glVertex3fv(c);
	glTexCoord2f(tx + 0.25, ty + 0);
	glVertex3fv(d);
	glEnd();
}
void drawmodel(char *objLocation)
{

	GLMmodel* obj = glmReadOBJ(objLocation);
	if (!obj) exit(0);
	glmUnitize(obj);
	glmFacetNormals(obj);
	glmVertexNormals(obj, 90.0);


	glmDraw(obj, GLM_SMOOTH | GLM_MATERIAL);
}

void desenhaVertical(GLuint texID)
{
	GLfloat vertices[][3] = { { -0.125,-0.75,-0.5 },
	{ 0.125,-0.75,-0.5 },
	{ 0.125,0.75,-0.5 },
	{ -0.125,0.75,-0.5 },
	{ -0.125,-0.75,0.5 },
	{ 0.125,-0.75,0.5 },
	{ 0.125,0.75,0.5 },
	{ -0.125,0.75,0.5 } };

	GLfloat normais[][3] = { { 0,0,-1 },
	{ 0,1,0 },
	{ -1,0,0 },
	{ 1,0,0 },
	{ 0,0,1 },
	{ 0,-1,0 } };

	GLfloat xa, xb, xc, xd, ya, yb, yc, yd;
	xa = 1; ya = 1;
	xb = 1; yb = 0;
	xc = 0; yc = 0;
	xd = 0; yd = 1;

	glBindTexture(GL_TEXTURE_2D, texID);
	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], xa, ya, xb, yb, xc, yc, xd, yd);
	glBindTexture(GL_TEXTURE_2D, NULL);
}
void desenhaHorizontal(GLuint texID)
{
	glPushMatrix();
	glRotatef(90, 0, 1, 0);
	desenhaVertical(texID);
	glPopMatrix();
}






void desenhaMeioBlocoVertic(GLuint texID)
{
	GLfloat vertices[][3] = { { -0.125,-0.75,-0.25 },
	{ 0.125,-0.75,-0.25 },
	{ 0.125,0.75,-0.25 },
	{ -0.125,0.75,-0.25 },
	{ -0.125,-0.75,0.25 },
	{ 0.125,-0.75,0.25 },
	{ 0.125,0.75,0.25 },
	{ -0.125,0.75,0.25 } };

	GLfloat normais[][3] = { { 0,0,-1 },
	{ 0,1,0 },
	{ -1,0,0 },
	{ 1,0,0 },
	{ 0,0,1 },
	{ 0,-1,0 } };

	GLfloat xa, xb, xc, xd, ya, yb, yc, yd;
	xa = 0.5; ya = 1;
	xb = 0.5; yb = 0;
	xc = 0; yc = 0;
	xd = 0; yd = 1;

	glBindTexture(GL_TEXTURE_2D, texID);
	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], xa, ya, xb, yb, xc, yc, xd, yd);
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaMetadeInferiorBlocoVertic(GLuint texID)
{
	GLfloat vertices[][3] = { { -0.125,-0.25,-0.5 },
	{ 0.125,-0.25,-0.5 },
	{ 0.125,0.25,-0.5 },
	{ -0.125,0.25,-0.5 },
	{ -0.125,-0.25,0.5 },
	{ 0.125,-0.25,0.5 },
	{ 0.125,0.25,0.5 },
	{ -0.125,0.25,0.5 } };

	GLfloat normais[][3] = { { 0,0,-1 },
	{ 0,1,0 },
	{ -1,0,0 },
	{ 1,0,0 },
	{ 0,0,1 },
	{ 0,-1,0 } };

	GLfloat xa, xb, xc, xd, ya, yb, yc, yd;
	xa = 1;
	ya = 0.25;
	xb = 1;
	yb = 0;
	xc = 0;
	yc = 0;
	xd = 0;
	yd = 0.25;

	glBindTexture(GL_TEXTURE_2D, texID);
	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], xa, ya, xb, yb, xc, yc, xd, yd);
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaMetadeSuperiorBlocoVertic(GLuint texID)
{
	GLfloat vertices[][3] = { { -0.125,-0.25,-0.5 },
	{ 0.125,-0.25,-0.5 },
	{ 0.125,0.25,-0.5 },
	{ -0.125,0.25,-0.5 },
	{ -0.125,-0.25,0.5 },
	{ 0.125,-0.25,0.5 },
	{ 0.125,0.25,0.5 },
	{ -0.125,0.25,0.5 } };

	GLfloat normais[][3] = { { 0,0,-1 },
	{ 0,1,0 },
	{ -1,0,0 },
	{ 1,0,0 },
	{ 0,0,1 },
	{ 0,-1,0 } };

	GLfloat xa, xb, xc, xd, ya, yb, yc, yd;
	xa = 1;
	ya = 1;
	xb = 1;
	yb = 0.75;
	xc = 0;
	yc = 0.75;
	xd = 0;
	yd = 1;

	glBindTexture(GL_TEXTURE_2D, texID);
	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], xa, ya, xb, yb, xc, yc, xd, yd);
	glBindTexture(GL_TEXTURE_2D, NULL);
}
void desenhaDegrau(GLuint texID)
{
	GLfloat vertices[][3] = { { -0.125,-0.01,-0.5 },
	{ 0.125,-0.01,-0.5 },
	{ 0.125,0.01,-0.5 },
	{ -0.125,0.01,-0.5 },
	{ -0.125,-0.01,0.5 },
	{ 0.125,-0.01,0.5 },
	{ 0.125,0.01,0.5 },
	{ -0.125,0.01,0.5 } };

	GLfloat normais[][3] = { { 0,0,-1 },
	{ 0,1,0 },
	{ -1,0,0 },
	{ 1,0,0 },
	{ 0,0,1 },
	{ 0,-1,0 } };

	GLfloat xa, xb, xc, xd, ya, yb, yc, yd;
	xa = 1; ya = 1;
	xb = 1; yb = 0;
	xc = 0; yc = 0;
	xd = 0; yd = 1;

	glBindTexture(GL_TEXTURE_2D, texID);
	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], xa, ya, xb, yb, xc, yc, xd, yd);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], xa, ya, xb, yb, xc, yc, xd, yd);
	glBindTexture(GL_TEXTURE_2D, NULL);
}





void desenhaParedeTinferior(GLuint texID)
{
	desenhaHorizontal(texID);

	glPushMatrix();
	glTranslatef(0.0, 0.0, 0.25);
	desenhaMeioBlocoVertic(texID);
	glPopMatrix();

}

void desenhaParedeTsuperior(GLuint texID)
{
	glPushMatrix();
	glRotatef(180, 0.0, 1.0, 0.0);
	desenhaParedeTinferior(texID);
	glPopMatrix();
}

void desenhaParedeTdireita(GLuint texID)
{
	glPushMatrix();
	glRotatef(90, 0.0, 1.0, 0.0);
	desenhaParedeTinferior(texID);
	glPopMatrix();
}

void desenhaParedeTesquerda(GLuint texID)
{
	glPushMatrix();
	glRotatef(-90, 0.0, 1.0, 0.0);
	desenhaParedeTinferior(texID);
	glPopMatrix();
}

void desenhaCantoSupEsq(GLuint texID)
{
	glPushMatrix();
	glTranslatef(0.25, 0.0, 0.0);
	glScaled(1.5,1.0, 1.0);
	glRotatef(90, 0.0, 1.0, 0.0);
	desenhaMeioBlocoVertic(texID);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0, 0.0, 0.25);
	desenhaMeioBlocoVertic(texID);
	glPopMatrix();
}

void desenhaCantoInfEsq(GLuint texID)
{
	glPushMatrix();
	glRotatef(90, 0.0, 1.0, 0.0);
	desenhaCantoSupEsq(texID);
	glPopMatrix();
}

void desenhaCantoSupDir(GLuint texID)
{
	glPushMatrix();
	glRotatef(-90, 0.0, 1.0, 0.0);
	desenhaCantoSupEsq(texID);
	glPopMatrix();
}

void desenhaCantoInfDir(GLuint texID)
{
	glPushMatrix();
	glRotatef(180, 0.0, 1.0, 0.0);
	desenhaCantoSupEsq(texID);
	glPopMatrix();
}

void desenhaJanela(GLuint texID, GLuint orientacao) {


	if (orientacao == 1) { //horizontal
		glPushMatrix();
		glRotatef(-90, 0, 1, 0);
	}
	glPushMatrix();
	glTranslatef(0.0, 0.5, 0.0);
	desenhaMetadeSuperiorBlocoVertic(texID);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0, -0.45, 0.0);
	desenhaMetadeInferiorBlocoVertic(texID);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0, 0.0, 0.0);
	glRotatef(180, 0, 1, 0);
	glScalef(0.85, 0.22, 0.85);
	drawmodel("Window/window.obj");
	glPopMatrix();
	if (orientacao == 1) {
		glPopMatrix();
	}
}


void desenhaCruz(GLuint texID)
{

	desenhaHorizontal(texID);
	desenhaVertical(texID);
}

void desenhaCubo(int tipo, GLuint texID)
{
	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
	{ 0.5,-0.5,-0.5 },
	{ 0.5,0.5,-0.5 },
	{ -0.5,0.5,-0.5 },
	{ -0.5,-0.5,0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };
	GLfloat normais[][3] = { { 0,0,-1 },
	{ 0,1,0 },
	{ -1,0,0 },
	{ 1,0,0 },
	{ 0,0,1 },
	{ 0,-1,0 } };


	GLfloat tx, ty;

	switch (tipo)
	{
	case 0: tx = 0, ty = 0;
		break;
	case 1: tx = 0, ty = 0.25;
		break;
	case 2: tx = 0, ty = 0.5;
		break;
	case 3: tx = 0, ty = 0.75;
		break;
	case 4: tx = 0.25, ty = 0;
		break;

	default:
		tx = 0.75, ty = 0.75;
	}
	glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], tx, ty);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], tx, ty);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], tx, ty);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], tx, ty);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], tx, ty);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], tx, ty);
	glBindTexture(GL_TEXTURE_2D, NULL);
}


void desenhaBussola(int width, int height, State &estado)
{

	// Altera viewport e projec��o

	glViewport(width - 60, 0, 60, 60);
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	gluOrtho2D(-30, 30, -30, 30);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_COLOR_MATERIAL);
	glRotatef(GRAUS(-estado.camera.getDir_long() + estado.camera.getDir_lat()) - 90, 0, 0, 1);

	glBegin(GL_TRIANGLES);
	glColor4f(0, 0, 0, 0.2);
	glVertex2f(0, 15);
	glVertex2f(-6, 0);
	glVertex2f(6, 0);
	glColor4f(1, 1, 1, 0.2);
	glVertex2f(6, 0);
	glVertex2f(-6, 0);
	glVertex2f(0, -15);
	glEnd();

	glLineWidth(1.0);
	glColor3f(1, 0.4, 0.4);
	strokeCenterString("N", 0, 20, 0, 0.1);
	strokeCenterString("S", 0, -20, 0, 0.1);
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);


	//rep�e projec��o chamando redisplay
	reshapeNavigateSubwindow(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

}

void desenhaModeloDir(Object obj, int width, int height)
{
	// Altera viewport e projec��o
	glViewport(width - 60, 0, 60, 60);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(-10, 10, -10, 10);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glRotatef(GRAUS(obj.getDir()), 0, 0, 1);

	glBegin(GL_QUADS);
	glColor4f(1, 0, 0, 0.5);
	glVertex2f(5, 2.5);
	glVertex2f(-10, 2.5);
	glVertex2f(-10, -2.5);
	glVertex2f(5, -2.5);
	glEnd();
	glBegin(GL_TRIANGLES);
	glVertex2f(10, 0);
	glVertex2f(5, 5);
	glVertex2f(5, -5);
	glEnd();

	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);

	//rep�e projec��o chamando redisplay
	redisplayTopSubwindow(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
}

void desenhaAngVisao(Camera *cam)
{
	GLfloat ratio;
	ratio = (GLfloat)glutGet(GLUT_WINDOW_WIDTH) / glutGet(GLUT_WINDOW_HEIGHT);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_FALSE);
	glPushMatrix();
	glTranslatef(cam->eye.getX(), OBJECTO_ALTURA, cam->eye.getZ());
	glColor4f(0, 0, 1, 0.2);
	glRotatef(GRAUS(cam->getDir_long()), 0, 1, 0);
	glBegin(GL_TRIANGLES);
	glVertex3f(0, 0, 0);
	glVertex3f(5 * cos(RAD(cam->getFov()*ratio*0.5)), 0, -5 * sin(RAD(cam->getFov()*ratio*0.5)));
	glVertex3f(5 * cos(RAD(cam->getFov()*ratio*0.5)), 0, 5 * sin(RAD(cam->getFov()*ratio*0.5)));
	glEnd();
	glPopMatrix();
	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}

void desenhaModelo(State &estado, Model &modelo)
{
	glColor3f(0, 1, 0);
	glutSolidCube(OBJECTO_ALTURA);
	glPushMatrix();
	glColor3f(1, 0, 0);
	glTranslatef(0, OBJECTO_ALTURA*0.75, 0);
	glRotatef(GRAUS(estado.camera.getDir_long() - modelo.objecto.getDir()), 0, 1, 0);
	glutSolidCube(OBJECTO_ALTURA*0.5);
	glPopMatrix();

}


void desenhaEscada(GLuint texID) {
	glPushMatrix();
	glTranslatef(0, -0.99, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.3, 0.14, 0);
	desenhaDegrau(texID);
	glTranslatef(0.5, 0.14, 0);
	desenhaDegrau(texID);


	glPopMatrix();
}
void desenhaImovel(GLuint texID, char mazedata[NUM_PISOS+1][MAZE_HEIGHT][MAZE_WIDTH + 1])
{
	int i, j, p;
	glColor3f(0.8f, 0.8f, 0.8f);
	glPushMatrix();
	glTranslatef(-MAZE_HEIGHT*0.5, 0.5, -MAZE_WIDTH*0.5);


	for (p = 0; p < NUM_PISOS; p++) {
		for (i = 0; i < MAZE_HEIGHT; i++) {
			for (j = 0; j < MAZE_WIDTH; j++) {
				glPushMatrix();
				glTranslatef(j, 0, i);

				if (mazedata[p][i][j] == '|')
				{
					//desenhaCubo((i + j)%6, texID);
					//desenhaParedeVertical(texID);
					desenhaVertical(texID);
				}
				else if (mazedata[p][i][j] == '-')
				{
					desenhaHorizontal(texID);
				}
				else if (mazedata[p][i][j] == '+')
				{
					desenhaCruz(texID);
				}
				else if (mazedata[p][i][j] == 'g')
				{
					desenhaParedeTinferior(texID);
				}
				else if (mazedata[p][i][j] == 't')
				{
					desenhaParedeTsuperior(texID);
				}
				else if (mazedata[p][i][j] == 'y')
				{
					desenhaParedeTdireita(texID);
				}
				else if (mazedata[p][i][j] == 'r')
				{
					desenhaParedeTesquerda(texID);
				}
				else if (mazedata[p][i][j] == 'a')
				{
					desenhaCantoSupEsq(texID);
				}
				else if (mazedata[p][i][j] == 'c')
				{
					desenhaCantoInfEsq(texID);
				}
				else if (mazedata[p][i][j] == 'b')
				{
					desenhaCantoSupDir(texID);
				}
				else if (mazedata[p][i][j] == 'd')
				{
					desenhaCantoInfDir(texID);
				}
				else if (mazedata[p][i][j] == '>')
				{
					glPushMatrix();
						//glTranslatef(-4.0, 0.0, 0.0);
						desenhaEscada(3);
					glPopMatrix();
				}
				else if (mazedata[p][i][j] == 'M')
				{
					if (p == 1) {
						glPushMatrix();
							glTranslatef(-1.1, -0.5, 0.4);
							glRotatef(90, 0.0, 1.0, 0.0);
							glScalef(0.5f, 0.5f, 0.5f);
							drawmodel("movel/Dresser.obj");
						glPopMatrix();
					}
					else if (p != 1) {
						glPushMatrix();
						glTranslatef(-1.1, -0.2, 0.4);
						glRotatef(90, 0.0, 1.0, 0.0);
						glScalef(0.5f, 0.5f, 0.5f);
						drawmodel("movel/Dresser.obj");
						glPopMatrix();
					}
				}
				else if (mazedata[p][i][j] == 'C')
				{
					if (p == 1) {
						glPushMatrix();
						glTranslatef(0.0, -0.3, 0.0);
						glScalef(0.5f, 0.5f, 0.5f);
						drawmodel("Bed/Bed.obj");
						glPopMatrix();
					}
					if (p != 1) {
						glPushMatrix();
						glScalef(0.5f, 0.5f, 0.5f);
						drawmodel("Bed/Bed.obj");
						glPopMatrix();
					}
				}
				else if (mazedata[p][i][j] == 'L')
				{
					glPushMatrix();
						glScalef(0.2f, 0.2f, 0.2f);
						glTranslatef(0.0, 3.0, 0.0);
						drawmodel("Lamp/lamp oblong.obj");
					glPopMatrix();
				}
				else if (mazedata[p][i][j] == 'S')
				{
					if (p == 1) {
						glPushMatrix();
						glScalef(0.7f, 0.7f, 0.7f);
						glTranslatef(0.0, -0.7, 0.0);
						drawmodel("Sofa/Sofa.obj");
						glPopMatrix();
					}
					else if (p != 1) {
						glPushMatrix();
						glScalef(0.7f, 0.7f, 0.7f);
						glTranslatef(0.0, -0.3, 0.0);
						drawmodel("Sofa/Sofa.obj");
						glPopMatrix();
					}
				}
				else if (mazedata[p][i][j] == 'P')
				{
					glPushMatrix();
						glScalef(2.8f, 2.2f, 2.8f);
						drawmodel("Door/Wooden_door/Wooden_door.obj");
					glPopMatrix();
				}
				else if (mazedata[p][i][j] == 'p')
				{
					glPushMatrix();
					glRotatef(90, 0.0, 1.0, 0.0);
					glScalef(2.8f, 2.2f, 2.8f);
					drawmodel("Door/Wooden_door/Wooden_door.obj");
					glPopMatrix();
				}
				else if (mazedata[p][i][j] == 'W')
				{
					if (mazedata[p][i][j - 1] == '|') {
						desenhaJanela(texID, 0);
					}
					else if (mazedata[p][i][j - 1] == '-')
					{
						desenhaJanela(texID, 1);
					}
					else
					{
						desenhaJanela(texID, 0);
					}

				}

				glPopMatrix();
				
			}
		}
		glTranslatef(0.0, 1.65, 0.0);
		//chamar desenhachao para desenhar novo piso
		
		
		
	}
	glPopMatrix();


}
void desenhaChao(GLfloat dimensao, GLuint texID, GLuint piso, Model &modelo)
{
	GLfloat i, j;
#define STEP    1
	
	GLint buracoEscadasI = modelo.posEscadas[0];
	GLint buracoEscadasJ = modelo.posEscadas[1];

	glBindTexture(GL_TEXTURE_2D, texID);

	glColor3f(0.5f, 0.5f, 0.5f);
	for (i = -dimensao / 2; i <= dimensao / 2; i += STEP)
		for (j = -dimensao / 2; j <= dimensao / 2; j += STEP)
		{
			if (piso == 1 && ((i == buracoEscadasI / 2 && j == (-buracoEscadasJ / 2) + 1) ||
				(i == (buracoEscadasI / 2) + 1 && j == (-buracoEscadasJ / 2) + 1) ||
				(i == (buracoEscadasI / 2) + 2 && j == (-buracoEscadasJ / 2) + 1) ||
				(i == (buracoEscadasI / 2) + 3 && j == (-buracoEscadasJ / 2) + 1) ||
				(i == (buracoEscadasI / 2) + 4 && j == (-buracoEscadasJ / 2) + 1) ||
				(i == buracoEscadasI / 2 && j == (-buracoEscadasJ / 2) + 2) ||
				(i == (buracoEscadasI / 2) + 1 && j == (-buracoEscadasJ / 2) + 2) ||
				(i == (buracoEscadasI / 2) + 2 && j == (-buracoEscadasJ / 2) + 2) ||
				(i == (buracoEscadasI / 2) + 3 && j == (-buracoEscadasJ / 2) + 2) ||
				(i == (buracoEscadasI / 2) + 4 && j == (-buracoEscadasJ / 2) + 2)))
			{
				glPushMatrix();
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glEnable(GL_BLEND);
				glBindTexture(GL_TEXTURE_2D, NULL);
				glColor4f(0.5f, 0.5f, 0.5f, 0.0f);
				glBegin(GL_POLYGON);
				glNormal3f(0, 1, 0);
				glTexCoord2f(1, 1);
				glVertex3f(i + STEP, 0, j + STEP);
				glTexCoord2f(0, 1);
				glVertex3f(i, 0, j + STEP);
				glTexCoord2f(0, 0);
				glVertex3f(i, 0, j);
				glTexCoord2f(1, 0);
				glVertex3f(i + STEP, 0, j);
				glEnd();
				glPopMatrix();
			}
			else {
				glBegin(GL_POLYGON);
				glNormal3f(0, 1, 0);
				glTexCoord2f(1, 1);
				glVertex3f(i + STEP, 0, j + STEP);
				glTexCoord2f(0, 1);
				glVertex3f(i, 0, j + STEP);
				glTexCoord2f(0, 0);
				glVertex3f(i, 0, j);
				glTexCoord2f(1, 0);
				glVertex3f(i + STEP, 0, j);
				glEnd();
			}
		}
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void desenhaTeto(GLfloat dimensao, GLuint texID, GLuint piso, Model &modelo)
{
	GLfloat i, j;
#define STEP    1

	GLint buracoEscadasI = modelo.posEscadas[0];
	GLint buracoEscadasJ = modelo.posEscadas[1];
	
	glTranslatef(0.0, 2, 0.0);
	glColor3f(0.5f, 0.5f, 0.5f);
	for (i = -dimensao / 2; i <= dimensao / 2; i += STEP)
		for (j = -dimensao / 2; j <= dimensao / 2; j += STEP)
		{

			if (piso == 0 && ((i == buracoEscadasI / 2 && j == (-buracoEscadasJ / 2) + 1) ||
				(i == (buracoEscadasI / 2) + 1 && j == (-buracoEscadasJ / 2) + 1) ||
				(i == (buracoEscadasI / 2) + 2 && j == (-buracoEscadasJ / 2) + 1) ||
				(i == (buracoEscadasI / 2) + 3 && j == (-buracoEscadasJ / 2) + 1) ||
				(i == (buracoEscadasI / 2) + 4 && j == (-buracoEscadasJ / 2) + 1) ||
				(i == buracoEscadasI / 2 && j == (-buracoEscadasJ / 2) + 2) ||
				(i == (buracoEscadasI / 2) + 1 && j == (-buracoEscadasJ / 2) + 2) ||
				(i == (buracoEscadasI / 2) + 2 && j == (-buracoEscadasJ / 2) + 2) ||
				(i == (buracoEscadasI / 2) + 3 && j == (-buracoEscadasJ / 2) + 2) ||
				(i == (buracoEscadasI / 2) + 4 && j == (-buracoEscadasJ / 2) + 2)))
			{
				glPushMatrix();
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glEnable(GL_BLEND);
				glBindTexture(GL_TEXTURE_2D, NULL);
				glColor4f(0.5f, 0.5f, 0.5f, 0.0f);
				glBegin(GL_POLYGON);
				glNormal3f(0, 1, 0);
				glTexCoord2f(1, 1);
				glVertex3f(i + STEP, 0, j + STEP);
				glTexCoord2f(0, 1);
				glVertex3f(i, 0, j + STEP);
				glTexCoord2f(0, 0);
				glVertex3f(i, 0, j);
				glTexCoord2f(1, 0);
				glVertex3f(i + STEP, 0, j);
				glEnd();
				glPopMatrix();
			}
			else {
				glDisable(GL_BLEND);
				glColor3f(0.5f, 0.5f, 0.5f);
				glBindTexture(GL_TEXTURE_2D, texID);
				glBegin(GL_POLYGON);
				glNormal3f(0, 1, 0);
				glTexCoord2f(1, 1);
				glVertex3f(i + STEP, 0, j + STEP);
				glTexCoord2f(0, 1);
				glVertex3f(i, 0, j + STEP);
				glTexCoord2f(0, 0);
				glVertex3f(i, 0, j);
				glTexCoord2f(1, 0);
				glVertex3f(i + STEP, 0, j);
				glEnd();
			}

		}
	glBindTexture(GL_TEXTURE_2D, NULL);
}

void importPlant(char * path, GLuint piso, Model &modelo, char mazedata[NUM_PISOS+1][MAZE_HEIGHT][MAZE_WIDTH + 1]) {
	int				yIndex, xIndex, tile = 0;
	FILE			*plant;

	/* Starting at x and y 0 */
	yIndex = 0;
	xIndex = 0;

	/* Load up the plant file */
	plant = fopen(path, "r+");

	/*
	* As long as our buffer is not pointing at the end of the
	* file, we'll keep reading characters from the file.
	*/
	while (tile != EOF) {
		/*
		* Read the character where our internal file position
		* indicator is, then move it to the next chararacter.
		* fgetc() returns an int, but that's okey since we can
		* assign decimal values to a char variable!
		*/
		tile = fgetc(plant);

		// Check if tile has reached the end of line
		if (tile == '\n') {
			yIndex++;
			xIndex = 0;
		}

		/*
		* This is where we add the character to our map array.
		* The map array is an array of chars, and the tile now holds an decimal value.
		*/

		if (tile == 'E') {
			modelo.posEscadas[0] = yIndex;
			modelo.posEscadas[1] = xIndex;
		}
		mazedata[piso][yIndex][xIndex] = tile;
		

		/*
		* Now that we've added one tile to the map array we'll want to advance to the next x position.
		*/
		xIndex++;
		
	}

	/* Close the level file */
	fclose(plant);
}

