#pragma once
#include "Position.h"
class Object
{
public:
	Object();
	Position    pos;
	GLfloat getDir();
	GLfloat getVel();

	void setPos(Position pos);
	void setDir(GLfloat dir);
	void setVel(GLfloat vel);

	void incDir(GLfloat dir);


	~Object();
private:
	
	GLfloat		dir;
	GLfloat		vel;
};

