#pragma once
#include "Position.h"


class Camera
{
public:
	Camera();
	Position    eye;
	GLfloat  getDir_long(); 
	GLfloat  getDir_lat();   
	GLfloat  getFov();


	void setDir_long(GLfloat dir_long);
	void incDir_long(GLfloat inc);

	void setDir_lat(GLfloat dir_lat);
	void incDir_lat(GLfloat dir_long);

	void setFov(GLfloat fov);
	void incFov(GLfloat inc);

	virtual ~Camera();
private:
	GLfloat  dir_long;  // longitude olhar (esq-dir)
	GLfloat  dir_lat;   // latitude olhar	(cima-baixo)
	GLfloat  fov;
};

