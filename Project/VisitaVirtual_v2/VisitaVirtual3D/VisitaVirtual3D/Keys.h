#pragma once
#include "Configs.h"
#include <stdio.h>
class Keys
{
public:
	Keys();
	GLboolean getUp();
	GLboolean getDown();
	GLboolean getLeft();
	GLboolean getRight();

	void setUp(GLboolean up);
	void setDown(GLboolean down);
	void setLeft(GLboolean left);
	void setRight(GLboolean right);

	virtual ~Keys();
private:
	GLboolean   up, down, left, right;
};
void imprime_ajuda();

