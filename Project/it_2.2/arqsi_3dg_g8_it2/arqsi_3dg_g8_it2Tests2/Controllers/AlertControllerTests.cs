﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RealEstateLibrary;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using RealEstateLibrary.Models;

namespace arqsi_3dg_g8_it2.Controllers.Tests
{
    [TestClass()]
    public class AlertControllerTests
    {

        private ApplicationDbContext _db = new ApplicationDbContext();
        private Sale _sale = new Sale() { Description = "sale" };
        private Buy _buy = new Buy() { Description = "buy" };
        private Exchange _exchange = new Exchange() { Description = "exchange" };
        private Rental _rental = new Rental() { Description = "rental" };
        private RealtyType _realtyType = new RealtyType() { Description = "Apartamento" };
        private RealtyType _realtyTypeMoradia = new RealtyType() { Description = "Moradia" };

        private Alert _Alert1 = new Alert() { };
        private Alert _Alert2 = new Alert() { };
        private Alert _Alert3 = new Alert() { };


        private ApplicationUser _admin = new ApplicationUser() { UserName = "1130351@isep.ipp.pt", Email = "1130351@isep.ipp.pt" };
        private ApplicationUser _user = new ApplicationUser() { UserName = "1070863@isep.ipp.pt", Email = "1130351@isep.ipp.pt" };

        public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
        {
            public ApplicationDbContext()
                : base("DefaultConnection")
            {
            }

            public DbSet<Ad> Ad { get; set; }
            public DbSet<AdType> AdType { get; set; }
            public DbSet<RealtyType> RealtyType { get; set; }
            public DbSet<Realty> Realty { get; set; }
            public DbSet<GPSLocation> GPSLocation { get; set; }
            public DbSet<Alert> Alert { get; set; }
            public DbSet<Photo> Photo { get; set; }

            public static ApplicationDbContext Create()
            {
                return new ApplicationDbContext();
            }

        }




        public void Configurations()
        {


            var store = new RoleStore<IdentityRole>(_db);
            var manager = new RoleManager<IdentityRole>(store);
            var roleAdmin = new IdentityRole { Name = "Admin" };

            manager.Create(roleAdmin);



            var store2 = new RoleStore<IdentityRole>(_db);
            var manager2 = new RoleManager<IdentityRole>(store2);
            var roleUser = new IdentityRole { Name = "User" };

            manager.Create(roleUser);



        }
        //Este teste serve para testar a gestão dos alertas. testa se eles são filtrados pelo userName 
        //de forma a que apenas o user que os cria os ve edita e apaga
        [TestMethod()]
        public void AssociationAlert_User()
        {
            Configurations();

            System.Collections.Generic.List<Alert> _Alerts = new System.Collections.Generic.List<Alert>();


            _Alert1.Address = "Porto";
            _Alert1.AdType = _sale;
            _Alert1.AdTypeID = _sale.AdTypeID;
            _Alert1.UserName = _user.UserName;

            _Alerts.Add(_Alert1);


            _Alert2.MaxCost = 1000;
            _Alert2.UserName = _admin.UserName;

            _Alerts.Add(_Alert1);


            _Alert3.Address = "Porto";
            _Alert3.AdType = _buy;
            _Alert3.AdTypeID = _buy.BuyID;
            _Alert3.UserName = _user.UserName;

            var alert = _Alerts.FindAll(x => x.UserName.Equals("1070863@isep.ipp.pt"));


            int resultexpect = 2;
            int result = alert.Count;

            Assert.IsTrue(result == resultexpect);
        }


    }
}