using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealEstateLibrary.Models
{
    public class Alert
    {
        [Key]
        public int AlertID { get; set; }

        [ForeignKey("AdType")]
        public int? AdTypeID { get; set; }
        public virtual AdType AdType { get; set; }

        [ForeignKey("RealtyType")]
        public int? RealtyTypeID { get; set; }
        public virtual RealtyType RealtyType { get; set; }

        public string Address { get; set; }
        public decimal? MaxCost { get; set; }
        public decimal? MaxArea { get; set; }

        public string UserName { get; set; }

    }
}