namespace RealEstateLibrary.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RealEstateLibrary.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RealEstateLibrary.Models.ApplicationDbContext context)
        {
            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Admin" };

                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "User"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "User" };

                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "Mediador"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Mediador" };

                manager.Create(role);
            }

            if (!context.Users.Any())
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);

                var user = new ApplicationUser() { UserName = "1130351@isep.ipp.pt", Email = "1130351@isep.ipp.pt" };
                manager.Create(user, "Pass_g8");
                manager.AddToRole(user.Id, "Admin");
            }

            if (!context.AdType.Any())
            {
                var sale = new Sale() { Description = "sale" };
                context.AdType.Add(sale);
                var buy = new Buy() { Description = "buy" };
                context.AdType.Add(buy);
                var exchange = new Exchange() { Description = "exchange" };
                context.AdType.Add(exchange);
                var rental = new Rental() { Description = "rental" };
                context.AdType.Add(rental);
            }
        }
    }
}
