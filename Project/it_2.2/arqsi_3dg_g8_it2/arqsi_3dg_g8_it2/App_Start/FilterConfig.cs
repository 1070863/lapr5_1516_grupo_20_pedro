﻿using System.Web;
using System.Web.Mvc;

namespace arqsi_3dg_g8_it2
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
