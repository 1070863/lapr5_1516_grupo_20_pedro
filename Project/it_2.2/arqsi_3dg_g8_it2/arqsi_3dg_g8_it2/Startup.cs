﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(arqsi_3dg_g8_it2.Startup))]
namespace arqsi_3dg_g8_it2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
